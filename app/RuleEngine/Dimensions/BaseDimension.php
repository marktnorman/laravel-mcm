<?php

namespace App\RuleEngine\Dimensions;

abstract class BaseDimension implements MetricInterface
{
    /**
     * @var
     */
    private $metric;

    public function setValue($metric)
    {
        $this->metric = $metric;
        return $this;
    }

    public function getValue()
    {
        // Getter
    }

    public function handle()
    {
        // Handle
    }
}