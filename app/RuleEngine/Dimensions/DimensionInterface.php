<?php
/**
 * Created by PhpStorm.
 * User: kylepretorius
 * Date: 13/03/2018
 * Time: 13:45
 */

namespace App\RuleEngine\Dimensions;

/**
 * Interface MetricInterface
 * @package App\RuleEngine\Metrics
 */
interface DimensionInterface
{

    /**
     * @param $value
     * @return mixed
     */
    public function setValue($value);

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @return mixed
     */
    public function handle();

}