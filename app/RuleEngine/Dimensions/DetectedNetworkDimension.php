<?php
/**
 * Created by PhpStorm.
 * User: kylepretorius
 * Date: 13/03/2018
 * Time: 11:27
 */

namespace App\RuleEngine\Dimensions;

use App\RuleEngine\Dimensions\DimensionInterface;

class DetectedNetworkDimension implements DimensionInterface
{
    /**
     * @var
     */
    private $dimension;

    public function __construct()
    {
        // __construct
    }

    public function setValue($dimension)
    {
        $this->dimension = $dimension;

        return $this;
    }

    public function getValue()
    {
        return $this->dimension;
    }

    public function handle()
    {

    }
}
