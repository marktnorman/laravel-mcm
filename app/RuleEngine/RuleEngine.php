<?php

namespace App\RuleEngine;

use App\RuleEngine\Metrics\DetectedNetworkMetricType;
use App\RuleEngine\Metrics\MsisdnMetricType;
use App\Contracts\Facades\McmLog;

/**
 * Class RuleFilter
 *
 * @package App\RuleEngine
 */
class RuleEngine
{
    /**
     * @var
     */
    private $flow;
    /**
     * @var
     */
    private $request;
    /**
     * @var
     */
    private $ruleCollection;
    /**
     * @var
     */
    private $detectionData;
    /**
     * @var
     */
    public $ruleEngineData;
    /**
     * @var array
     */
    private $metricValues;

    /**
     * RuleEngine constructor.
     *
     * @param \Illuminate\Database\Eloquent\Collection $ruleCollection
     * @param                                          $detectionData
     */
    public function __construct($request, $ruleCollection, $detectionData)
    {
        $this->request = $request;
        $this->ruleCollection = $ruleCollection->toArray();
        $this->detectionData = $detectionData;
        $this->metricValues = [];
        $this->flow = '';
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        // Iterate rules
        foreach ($this->ruleCollection as $rule) {

            // If a rule is valid then break from iterating rules and return the flow
            if ($this->processConditions($rule['rule_condition']) == true) {

                // If rule has flow add to a new object attached to the request called ruleEngineData
                if (null !== $rule['flow_class_name']) {
                    $this->flow = $rule['flow_class_name'];
                    $this->ruleEngineData['detectedRule']['flow'] = $this->flow;
                    $this->request->attributes->add(['ruleEngineData' => (Object)$this->ruleEngineData]);
                }

                break;
            }
        }

        return $this->request;
    }

    /**
     * @param $ruleConditions
     *
     * @return bool
     */
    private function processConditions($ruleConditions)
    {
        // Rule is assumed invalid by default before conditions are processed
        $validRule = false;

        foreach ($ruleConditions as $condition) {
            $metricValue = $this->getMetricValue($condition['rule_metric_type']['class_name']);
            $class = 'App\\RuleEngine\\Conditions\\' . $condition['rule_condition_type']['class_name'];
            if (class_exists($class)) {
                $processedCondition = new $class($metricValue, $condition['value']);
                $validRule = $processedCondition->getResult();

                // If a rule condition ever evaluates to false then we can continue the foreach confition iteration
                // otherwise break out with true found condition value
                if (!$validRule) {
                    continue;
                } else {
                    break;
                }
            } else {
                McmLog::warning('flow', 'Rule condition type class not found.', [
                    'message_context' => $class,
                ]);
            }
        }

        return $validRule;
    }

    /**
     * @param $metric
     *
     * @return mixed
     */
    private function getMetricValue($metric)
    {
        // If the index is not set then we have not yet created
        if (!isset($this->metricValues["{$metric}"])) {
            $setMethodName = 'set' . $metric;
            $this->metricValues["{$metric}"] = $this->{$setMethodName}();
        }

        return $this->metricValues["{$metric}"];
    }

    // TODO: Provide all metric data in the detectionData object so that we can dynamically
    // TODO: instantiate the metric classes rather than have a separate set method for
    // TODO: each metric like we are currently doing below.

    /**
     * Being called dynamically from the getMetricValue method
     *
     * @return mixed
     */
    private function setDetectedNetworkMetricType()
    {
        $detectedNetworkValue = (new DetectedNetworkMetricType($this->detectionData))->getValue();

        return $detectedNetworkValue;
    }

    /**
     * Being called dynamically from the getMetricValue method
     *
     * @return mixed
     */
    private function setMsisdnMetricType()
    {
        $msisdnValue = (new MsisdnMetricType($this->detectionData))->getValue();

        return $msisdnValue;
    }
}