<?php
/**
 * Created by PhpStorm.
 * User: kylepretorius
 * Date: 13/03/2018
 * Time: 11:27
 */

namespace App\RuleEngine\Metrics;

class MsisdnMcryptMetric implements MetricInterface
{

    /**
     * @var
     */
    private $metric;

    public function __construct() { }

    public function setValue($metric)
    {
        $this->metric = $metric;
        return $this;
    }

    public function getValue()
    {
        return $this->metric;
    }

    public function handle()
    {
        //process logic
    }
}