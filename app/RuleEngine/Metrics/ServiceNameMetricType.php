<?php
/**
 * Created by PhpStorm.
 * User: glencampbell
 * Date: 2018/04/19
 * Time: 06:43
 */

namespace App\RuleEngine\Metrics;

class ServiceNameMetricType implements MetricInterface
{

    /**
     * @var
     */
    private $metric;

    public function __construct($detectionData)
    {
        // __construct
    }

    public function setValue($metric)
    {
        $this->metric = $metric;

        return $this;
    }

    public function getValue()
    {
        return $this->metric;
    }

    public function handle()
    {

    }
}