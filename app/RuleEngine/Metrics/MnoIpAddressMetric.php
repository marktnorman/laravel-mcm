<?php

namespace App\RuleEngine\Metrics;

class MnoIpAddressMetric extends BaseMetric
{

    private $metric;

    public function __construct() { }

    public function setValue($metric)
    {
        $this->metric = $metric;
        return $this;
    }

    public function getValue()
    {
        return $this->metric;
    }


    public function handle()
    {
        //process logic
    }


}