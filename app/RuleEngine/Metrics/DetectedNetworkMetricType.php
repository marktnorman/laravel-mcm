<?php
/**
 * Created by PhpStorm.
 * User: glencampbell
 * Date: 2018/04/19
 * Time: 06:43
 */

namespace App\RuleEngine\Metrics;

use App\Helpers\Helpers;

class DetectedNetworkMetricType extends BaseMetric
{
    private $metric;
    private $detectionData;

    /**
     * DetectedNetworkMetricType constructor.
     *
     * @param $detectionData
     */
    public function __construct($detectionData)
    {
        $this->detectionData = $detectionData;
        $this->handle();
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->metric;
    }

    /**
     * Retrieve the detected network from the detection data object
     */
    public function handle()
    {
        foreach ($this->detectionData->msisdnNetworkData as $key => $slug) {
            $this->metric = $slug;
        }
    }
}