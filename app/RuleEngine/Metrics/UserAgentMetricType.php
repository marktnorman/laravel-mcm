<?php

namespace App\RuleEngine\Metrics;

class UserAgentMetricType implements MetricInterface
{

    /**
     * @var
     */
    private $metric;

    public function __construct($detectionData)
    {
        // __construct
    }

    public function setValue($metric)
    {
        $this->metric = $metric;

        return $this;
    }

    public function getValue()
    {
        return $this->metric;
    }


    public function handle()
    {

    }
}