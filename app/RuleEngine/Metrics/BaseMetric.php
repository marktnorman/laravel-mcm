<?php
/**
 * Created by PhpStorm.
 * User: kylepretorius
 * Date: 13/03/2018
 * Time: 11:27
 */

namespace App\RuleEngine\Metrics;

abstract class BaseMetric implements MetricInterface
{
    private $metric;

    public function setValue($metric)
    {
    }

    public function getValue()
    {
        return $this->metric;
    }

    public function handle()
    {
        // Handle logic to set the metric value
    }
}