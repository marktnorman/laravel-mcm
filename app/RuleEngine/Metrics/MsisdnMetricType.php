<?php

namespace App\RuleEngine\Metrics;

use App\Helpers\Helpers;

class MsisdnMetricType extends BaseMetric
{
    private $metric;
    private $detectionData;

    /**
     * MsisdnMetricType constructor.
     *
     * @param $detectionData
     */
    public function __construct($detectionData) {
        $this->detectionData = $detectionData;
        $this->handle();
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->metric;
    }

    /**
     * Retrieve the msisdn from the detection data object
     */
    public function handle()
    {
        $msisdn_value = '';
        Helpers::arrayKeySearch($this->detectionData, 'msisdn', $msisdn_value);
        $this->metric = $msisdn_value;
    }
}