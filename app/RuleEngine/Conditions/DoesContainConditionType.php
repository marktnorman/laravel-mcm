<?php

namespace App\RuleEngine\Conditions;

class DoesContainConditionType extends BaseCondition
{
    /**
     * The DoesContainConditionType checks if our metric value is contained in the our value array
     */
    public function handle()
    {
        $valueArray = array_map('trim', explode(',', $this->value));
        $this->result = (in_array($this->metric, $valueArray)) ? true : false;
    }
}