<?php

namespace App\RuleEngine\Conditions;

class IsNotConditionType extends BaseCondition
{
    /**
     * The IsNotConditionType simple check if the metric value is not equal to the condition value
     */
    public function handle()
    {
        $this->result = ($this->metric != $this->value) ? true : false;
    }
}