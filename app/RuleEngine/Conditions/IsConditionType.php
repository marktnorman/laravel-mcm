<?php

namespace App\RuleEngine\Conditions;

class IsConditionType extends BaseCondition
{
    /**
     * The IsConditionType simple check if the metric value is equal to the condition value
     */
    public function handle()
    {
        $this->result = ($this->metric == $this->value) ? true : false;
    }
}