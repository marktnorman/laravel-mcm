<?php

namespace App\RuleEngine\Conditions;

abstract class BaseCondition implements ConditionInterface
{
    protected $metric;
    protected $value;
    protected $result;

    /**
     * BaseCondition constructor.
     *
     * @param $metric
     * @param $value
     */
    public function __construct($metric, $value) {
        $this->metric = $metric;
        $this->value = $value;
        $this->handle();
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * This method will be overridden by condition classes that extend this base class
     */
    public function handle()
    {
        // Condition Logic
    }
}