<?php

namespace App\RuleEngine\Conditions;

/**
 * Interface MetricInterface
 * @package App\RuleEngine\Conditions
 */
interface ConditionInterface
{
    /**
     * ConditionInterface constructor.
     *
     * @param $metric
     * @param $value
     */
    public function __construct($metric, $value);

    /**
     * @return mixed
     */
    public function getResult();

    /**
     * @return mixed
     */
    public function handle();

}