<?php

namespace App\RuleEngine\Flows;

use App\Models\BillingChannel;
use Illuminate\Support\Facades\DB;

/**
 * Class slingshotFlow
 *
 * @package App\RuleEngine\Flows
 */
class slingshotFlow
{
    /**
     * @var
     */
    private $request;

    /**
     * @var
     */
    private $network;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * This Slingshot flow will grab the required billing channel config and route user off to determined location
     */
    public function handle()
    {
        $row = new BillingChannel;
        $response = $row->billingChannelRow($this->network);
        if ($response !== null) {
            $data = [
                'callback' => url()->current(),
                't'        => $response->slingshot_token,
                'c'        => $response->slingshot_cypher_key,
            ];
            $payload = json_encode(['payload' => $data]);
            $queryArgs = '?p=' . $payload;

            return redirect()->away($response->slingshot_endpoint . $queryArgs);
        } else {
            McmLog::warning('flow', 'No billing channel found for specfied network.', [
                'network' => $this->network,
            ]);
        }
    }
}