<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RuleCondition
 * @package App\Models
 */
class RuleCondition extends Model
{

    /**
     * @var string
     */
    protected $table = 'rule_conditions';

    /**
     * @var array
     */
    protected $fillable = [
        'rule_id',
        'operator',
        'metric_id',
        'condition_id',
        'dimension_id',
        'value'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rule()
    {
        return $this->belongsToMany(Rule::class, "rule_id")->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ruleConditionType()
    {
        return $this->hasOne(RuleConditionType::class, "id", "condition_type_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ruleMetricType()
    {
        return $this->hasOne(RuleMetricType::class, "id", "metric_type_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ruleDimensionType()
    {
        return $this->hasOne(RuleDimensionType::class, "id", "dimension_type_id");
    }

}
