<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Service
 *
 * @package App\Models
 */
class Service extends Model
{

    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'country_id',
        'name',
        'campaign_keyword',
        'service_cost',
        'service_copy',
        'display_title',
        'post_doi_landing_url',
        'forgot_password_url',
        'double_optin',
        'display_interstitial',
        'template_id',
        'active',
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function billingChannels()
    {
        return $this->belongsToMany(BillingChannel::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function template()
    {
        return $this->belongsTo(Template::class, 'template_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function banner()
    {
        return $this->belongsToMany(Banner::class)->withTimestamps();
    }
}
