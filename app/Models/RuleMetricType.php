<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RuleMetricType
 * @package App\Models
 *
 *
 * @SWG\Definition(
 *     definition="RuleMetricType",
 *     required={"name", "class_name"},
 *     @SWG\Property(
 *          property="id",
 *          type="int",
 *          description="Metric type attribute ID",
 *          example="1"
 *    ),
 *     @SWG\Property(
 *          property="name",
 *          type="string",
 *          description="Metric type attribute name",
 *          example="mno_ip_addresses"
 *    ),
 *     @SWG\Property(
 *          property="created_at",
 *          type="string",
 *          format="date-time",
 *          description="Metric type attribute created date and time",
 *          example="2018-04-09 06:31:07"
 *    ),
 *     @SWG\Property(
 *          property="updated_at",
 *          type="string",
 *          format="date-time",
 *          description="Metric type attribute updated date and time",
 *          example="2018-04-09 08:41:12"
 *    )
 * )
 */

class RuleMetricType extends Model
{
    /**
     * @var string
     */
    protected $table = 'rule_metric_types';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'class_name',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ruleCondition()
    {
        return $this->belongsTo(RuleCondition::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function conditionTypes()
    {
        return $this->belongsToMany(RuleConditionType::class)->withTimestamps();
    }

}
