<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerVariationType extends Model
{
    protected $fillable = [
        'name',
        'min_width',
        'max_width',
        'min_height',
        'max_height',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bannerVariation()
    {
        return $this->belongsTo(BannerVariation::class, "banner_variation_type_id");
    }
}
