<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class BillingChannel
 *
 * @package App\Models
 */
class BillingChannel extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'active',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function networks()
    {
        return $this->belongsToMany(Network::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function billingChannelRow($network)
    {
        return DB::table('billing_channels')->where('name', $network)->first();
    }
}
