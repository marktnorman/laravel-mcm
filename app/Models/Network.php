<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Network
 *
 * @package App\Models
 */
class Network extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'active',
        'ip_range',
        'slingshot_enabled',
        'slingshot_url',
        'slingshot_token',
        'slingshot_cypher_key',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function billingChannels()
    {
        return $this->belongsToMany(BillingChannel::class)->withTimestamps();
    }

}
