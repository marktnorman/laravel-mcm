<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 *
 * @package App\Models
 * @author  Vince Kruger <vincekruger@gmail.com>
 */
class Country extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'country_iso_code',
        'country_dialing_code',
        'name',
        'title',
    ];
}
