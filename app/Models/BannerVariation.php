<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BannerVariation
 * @package App\Models
 */
class BannerVariation extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'banner_id',
        'width',
        'height',
        'location',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function banner()
    {
        return $this->belongsToMany(Banner::class, "banner_id")->withTimestamps();
    }
}
