<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RuleDimensionType
 * @package App\Models
 *
 *
 * @SWG\Definition(
 *     definition="RuleDimensionType",
 *     required={"name", "class_name"},
 *     @SWG\Property(
 *          property="id",
 *          type="int",
 *          description="Dimension type attribute ID",
 *          example="2"
 *    ),
 *     @SWG\Property(
 *          property="metric_type_id",
 *          type="int",
 *          description="Dimension associated metric type attribute ID",
 *          example="2"
 *    ),
 *     @SWG\Property(
 *          property="name",
 *          type="string",
 *          description="Dimension type attribute name",
 *          example="detected_network"
 *    ),
 *     @SWG\Property(
 *          property="class_name",
 *          type="string",
 *          description="Dimension type attribute class name",
 *          example="DetectedNetworkDimensionType"
 *    ),
 *     @SWG\Property(
 *          property="created_at",
 *          type="string",
 *          format="date-time",
 *          description="Metric type attribute created date and time",
 *          example="2018-04-09 06:31:07"
 *    ),
 *     @SWG\Property(
 *          property="updated_at",
 *          type="string",
 *          format="date-time",
 *          description="Metric type attribute updated date and time",
 *          example="2018-04-09 08:41:12"
 *    )
 * )
 */

class RuleDimensionType extends Model
{
    /**
     * @var string
     */
    protected $table = 'rule_dimension_types';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'class_name',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ruleCondition()
    {
        return $this->belongsToMany(RuleCondition::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ruleMetricType()
    {
        return $this->belongsToMany(RuleMetricType::class, "metric_type_id")->withTimestamps();
    }

}
