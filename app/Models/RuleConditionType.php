<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RuleConditionType
 * @package App\Models
 *
 *
 * @SWG\Definition(
 *     definition="RuleConditionType",
 *     required={"name", "class_name", "has_value"},
 *     @SWG\Property(
 *          property="rule_metric_type_id",
 *          type="int",
 *          description="Dimension pivot data object rule_metric_type_id",
 *          example="1"
 *    ),
 *     @SWG\Property(
 *          property="rule_condition_type_id",
 *          type="int",
 *          description="Dimension pivot data object rule_condition_type_id",
 *          example="1"
 *    ),
 *     @SWG\Property(
 *          property="created_at",
 *          type="string",
 *          format="date-time",
 *          description="Dimension pivot data object created date and time",
 *          example="2018-04-09 06:31:07"
 *    ),
 *     @SWG\Property(
 *          property="updated_at",
 *          type="string",
 *          format="date-time",
 *          description="Dimension pivot data object updated date and time",
 *          example="2018-04-09 08:41:12"
 *    ),
 * )
 */

class RuleConditionType extends Model
{
    protected $table = 'rule_condition_types';

    protected $fillable = [
        'name',
        'class_name',
        'has_value',
    ];

    public function ruleCondition()
    {
        return $this->belongsTo(RuleCondition::class);
    }

}
