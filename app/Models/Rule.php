<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rule
 * @package App\Models
 */
class Rule extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'rule_name',
        'rule_slug',
        'level',
        'flow',
        'template',
        'weight',
        'active',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ruleCondition()
    {
        return $this->hasMany(RuleCondition::class);
    }
}
