<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'theme_id',
        'upsell',
    ];

    /**
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function theme()
    {
        return $this->hasOne(Theme::class);
    }
}
