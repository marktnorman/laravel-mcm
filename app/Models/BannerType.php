<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BannerType
 * @package App\Models
 */
class BannerType extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'active',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function banner()
    {
        return $this->belongsTo(Banner::class, "banner_type_id");
    }
}
