<?php

namespace App\Http\Middleware;


use App\RuleEngine\RuleEngine;
use App\Models\Rule;
use Closure;
use utilphp\util;
use App\Contracts\Facades\McmLog;

/**
 * Class RuleEngineMiddleware
 *
 * @package App\Http\Middleware
 */
class RuleEngineMiddleware
{
    public function handle($request, Closure $next)
    {
        // Get active rules with its condition / metric and dimensions
        $activeRules = Rule::with('ruleCondition',
            'ruleCondition.ruleConditionType',
            'ruleCondition.ruleMetricType',
            'ruleCondition.ruleDimensionType')
                           ->where('rules.active', 1)
                           ->orderBy('weight', 'DESC')
                           ->get();

        // Go through records to store as associative array for rule engine class to store keys
        // we either create an array here to use in object and deconstruct it there as single rule
        // or we handle each condition/dimension/metric individually (this needs to keep in operator in mind)
        $detectionData = $request->attributes->get('detectionData');

        if (!empty($activeRules)) {
            // $activeRules passed through as Collection
            $ruleFilter = new RuleEngine($request, $activeRules, $detectionData);
            $ruleFilter->handle();
        }

        return $next($request);
    }

}
