<?php
/**
 * Created by PhpStorm.
 * User: kylepretorius
 * Date: 22/02/2018
 * Time: 08:46
 */

namespace App\Http\Middleware;

use App\Http\Api\ApiController;
use Closure;
use Dingo\Api\Http\Request;
use Exception;

class AuthPayloadRequest extends ApiController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try
        {
            if(empty($request->header('Authorization')))
            {
                return $this->respondWithError('Public Key not set!');
            }

            if(!empty($request->get('source')))
            {
                $apiKey = env(strtoupper($request->get('source')) . '_API_TOKEN','');

                if($request->header('Authorization') !== $apiKey)
                {
                    return $this->respondUnauthorized('Token is Invalid');
                }
            }

        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
            {
                return $this->respondUnauthorized('Token is Invalid');
            } else {
                return $this->respondWithError('Something is wrong');
            }
        }
        return $next($request);
    }
}