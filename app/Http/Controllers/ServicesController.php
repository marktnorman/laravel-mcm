<?php

namespace App\Http\Controllers;

use App\Http\Resources\ServiceResource;
use App\Models\Country;
use App\Models\Service;
use Illuminate\Http\Request;
use utilphp\util;

/**
 * Class ServicesController
 *
 * @package App\Http\Controllers
 */
class ServicesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param                          $serviceId
     */
    public function handle(Request $request, $serviceId)
    {
        // If we find that the flow is set then lets instantiate the respective class to handle the client
        if (null !== $request->attributes->get('ruleEngineData')->detectedRule['flow']) {
            $flowClassName = $request->attributes->get('ruleEngineData')->detectedRule['flow'];
            $class = 'App\\RuleEngine\\Flows\\' . $flowClassName;
            if (class_exists($class)) {
                $processedFlow = new $class($request);
                $processedFlow->handle();
            } else {
                McmLog::warning('flow', 'Rule engine flow class not found.', [
                    'flow_class_name' => $class,
                ]);
            }
        }
    }
}
