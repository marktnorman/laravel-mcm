<?php
/**
 * Created by PhpStorm.
 * User: kylepretorius
 * Date: 22/02/2018
 * Time: 08:51
 */


namespace App\Http\Controllers;

use App\Http\Controller;
use Illuminate\Http\Response;
use App\Contracts\Facades\McmLog;

class ApiController extends Controller
{

    protected $statusCode = Response::HTTP_OK;

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     *
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param       $data
     * @param array $headers
     *
     * @return mixed
     */
    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * @param $resource
     *
     * @return mixed
     */
    public function respondWithSuccess($resource = [])
    {
        return $this->respond($resource);
    }

    /**
     * @param $message
     *
     * @return mixed
     */
    public function respondWithError($message)
    {
        return $this->respond([
            'errors' => [
                'status' => $this->getStatusCode(),
                'errors' => $message,
            ],
        ]);
    }

    /**
     * @return mixed
     *
     * @param $message
     */
    public function respondUnauthorized($message = 'The requested resource failed authorization')
    {
        $this->setStatusCode(Response::HTTP_UNAUTHORIZED);

        McmLog::error("api", $message, [
            'status_code' => $this->statusCode,
            'request_url' => parse_url($_SERVER['REQUEST_URI'])['path']
        ]);

        return $this->respondWithError($message);
    }

    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondNotFound($message = 'The requested resource could not be found')
    {
        $this->setStatusCode(Response::HTTP_NOT_FOUND);

        McmLog::error("api", $message, [
            'status_code' => $this->statusCode,
            'request_url' => parse_url($_SERVER['REQUEST_URI'])['path']
        ]);

        return $this->respondWithError($message);
    }


    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondInternalError($message = 'An internal server error has occurred')
    {
        $this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

        McmLog::critical("api", $message, [
            'status_code' => $this->statusCode,
            'request_url' => parse_url($_SERVER['REQUEST_URI'])['path']
        ]);

        return $this->respondWithError($message);
    }

    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondUnprocessableEntity($message = 'The request cannot be processed with the given parameters')
    {
        $this->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);

        McmLog::error("api", $message, [
            'status_code' => $this->statusCode,
            'request_url' => parse_url($_SERVER['REQUEST_URI'])['path']
        ]);

        return $this->respondWithError($message);
    }


    /**
     * @param array $resource
     *
     * @return mixed
     */
    public function respondCreated($resource = [])
    {
        return $this->setStatusCode(Response::HTTP_CREATED)->respondWithSuccess($resource);
    }


    /**
     * @param array $resource
     *
     * @return mixed
     */
    public function respondUpdated($resource = [])
    {
        return $this->setStatusCode(Response::HTTP_OK)->respondWithSuccess($resource);
    }

    /**
     * @return mixed
     */
    public function respondNoContent()
    {
        return $this->setStatusCode(Response::HTTP_NO_CONTENT)->respondWithSuccess();
    }

    /**
     * @param null $message
     *
     * @return mixed
     */
    public function respondHttpConflict($message = 'The request cannot be processed due to HTTP conflict')
    {
        $this->setStatusCode(Response::HTTP_CONFLICT);

        McmLog::error("api", $message, [
            'status_code' => $this->statusCode,
            'request_url' => parse_url($_SERVER['REQUEST_URI'])['path']
        ]);

        return $this->respondWithError($message);
    }

}