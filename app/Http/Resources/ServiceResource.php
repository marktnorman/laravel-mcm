<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ServiceResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attributes = array_only($this->getAttributes(), $this->getFillable());
        $attributes['sdp_service_id'] = $this->sdp_service_id;

        foreach ($attributes as $key => $value) {
            if ( is_null($value) ) {
                $attributes[$key] = '';
            }
        }

        return [
            'id' => $this->id,
            'type' => 'Service',
            'attributes' => $attributes
        ];
    }
}
