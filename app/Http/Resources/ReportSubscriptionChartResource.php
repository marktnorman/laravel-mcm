<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class ReportSubscriptionChartResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'x_axis_day' => Carbon::parse($this->date)->day,
            'x_axis_date' => Carbon::parse($this->date)->format('jS F \'y'),
            'y_axis_attempts' => $this->attempts,
            'y_axis_confirmed' => $this->confirmed
        ];
    }
}
