<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReportSubscriptionChartCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => 'Report Subscription Chart',
            'keys' => [
                'x_axis' => 'day',
                'y_axis' => 'user_subscriptions'
            ],
            'data' => ReportSubscriptionChartResource::collection($this->collection)
        ];
    }
}
