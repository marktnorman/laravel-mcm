<?php

namespace App\Http\Resources;

use App\Helpers\FormatString;
use Illuminate\Http\Resources\Json\Resource;

class ReportSubscriptionByServiceResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'Report Subscription By Service',
            'attributes' => [
                'service_name' => $this->service_name,
                'attempts' => FormatString::numberReadable($this->attempts, 0),
                'confirmed' => FormatString::numberReadable($this->confirmed, 0),
                'impressions' => FormatString::numberReadable($this->impressions, 0),
                'cvr_impressions' => FormatString::numberPercentage($this->cvr_impressions),
                'dates' => new ReportSubscriptionByDateCollection($this->dates)
            ]
        ];
    }
}
