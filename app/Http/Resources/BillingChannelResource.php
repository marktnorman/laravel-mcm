<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class BillingChannelResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attributes = array_only($this->getAttributes(), $this->getFillable());

        foreach ($attributes as $key => $value) {
            if ( is_null($value) ) {
                $attributes[$key] = '';
            }
        }

        return [
            'id' => $this->id,
            'type' => 'Billing Channel',
            'attributes' => $attributes
        ];
    }
}
