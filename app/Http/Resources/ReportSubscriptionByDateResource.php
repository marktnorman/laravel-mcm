<?php

namespace App\Http\Resources;

use App\Helpers\FormatString;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class ReportSubscriptionByDateResource
 * @package App\Http\Resources
 */
class ReportSubscriptionByDateResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'Report Subscription By Date',
            'attributes' => [
                'date' => Carbon::parse($this->date)->format('jS F \'y'),
                'attempts' => FormatString::numberReadable($this->attempts, 0),
                'confirmed' => FormatString::numberReadable($this->confirmed, 0),
                'impressions' => FormatString::numberReadable($this->impressions, 0),
                'cvr_impressions' => FormatString::numberPercentage($this->cvr_impressions)
            ]
        ];
    }
}
