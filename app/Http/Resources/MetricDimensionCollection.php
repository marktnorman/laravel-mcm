<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MetricDimensionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'data' => MetricDimensionsResource::collection($this->collection),
            ],
            'meta' => [
                'type' => 'Metric Dimensions'
            ]
        ];
    }
}
