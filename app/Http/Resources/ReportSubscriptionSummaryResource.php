<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ReportSubscriptionSummaryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'Report Subscription Summary',
            'attributes' => [
                'title' => $this->title,
                'value' => $this->value,
                'increase' => $this->increase ? '1' : '0'
            ]
        ];
    }
}
