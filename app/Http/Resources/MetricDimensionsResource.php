<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MetricDimensionsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'attributes' => [
                $this->resource,
            ],
            'id' => $this->id,
            'type' => 'Metric Dimensions',
        ];
    }
}
