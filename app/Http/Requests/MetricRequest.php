<?php

namespace App\Http\Requests;

use Dingo\Api\Http\FormRequest;



class MetricRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'metric_type_id' => 'required|numeric|min:1',
        ];
    }

    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'metric_type_id' => $this->route('metric_type_id'),
        ]);
    }



}
