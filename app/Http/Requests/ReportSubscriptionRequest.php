<?php

namespace App\Http\Requests;

use Dingo\Api\Http\FormRequest;

class ReportSubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "country_id" => "sometimes",
            "billingChannel_id" => "sometimes",
            "service_id" => "sometimes",
            "start_date" => "required",
            "end_date" => "required",
        ];
    }
}
