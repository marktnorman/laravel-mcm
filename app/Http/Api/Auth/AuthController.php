<?php
namespace App\Http\Api\Auth;

use App\Http\Api\ApiController;
use App\Http\Requests\LoginRequest;
use Dingo\Api\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Auth Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling user registration through the means of email verification and
    | authenticating a user's session - logging in - by creating a JWT token that will be used client side.
    |
    */

    /**
     * Logs a user in
     *
     * @param LoginRequest $request
     * @return mixed
     */
    public function login(LoginRequest $request)
    {
        // Inputs
        $credentials = $request->only('email', 'password');

        try {
            // Attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->respondUnauthorized('Invalid Credentials. Please make sure you entered
                the right information and you have verified your email address.');
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return $this->respondInternalError('Token could not be created.');
        }

        // Return token
        return $this->respondWithSuccess([
            'token' => $token
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/auth/login",
     *     tags={"Login"},
     *     operationId="get_jwt_auth_token",
     *     summary="Login to API",
     *     description="Login to API by utilising JWT auth for a token",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="email",
     *          description="User email address",
     *          required=true,
     *          type="string",
     *          format="URL Params",
     *          in="formData"
     *     ),
     *     @SWG\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string",
     *          format="URL Params",
     *          in="formData"
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Returns JSON formatted JWT auth token",
     *          @SWG\Schema(ref="#/definitions/JwtAuth")
     *     ),
     *     @SWG\Response(
     *            response=401,
     *            description="Invalid Credentials. Please make sure you entered the right information and you have verified your email address.",
     *     )
     * )
     */
}