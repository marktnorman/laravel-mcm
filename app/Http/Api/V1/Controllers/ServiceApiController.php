<?php

namespace App\Http\Api\V1\Controllers;

use App\Helpers\SlackNotifier;
use App\Http\Api\ApiController;
use App\Http\Resources\AbstractCollection;
use App\Http\V1\Requests\ServicesSdpRequest;
use App\Jobs\ProcessSDPService;
use App\Models\Service;
use Carbon\Carbon;
use Dingo\Api\Contract\Http\Request;
use App\Contracts\Facades\McmLog;


/**
 * Class MetricController
 *
 * @package App\Http\Api\V1\Controllers
 */
class ServiceApiController extends ApiController
{
    /**
     * @return string
     */
    public function index()
    {
        try {
            $serviceAll = Service::all();
            $filtered = new AbstractCollection($serviceAll);

            return $this->respondWithSuccess($filtered);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     */
    public function processSDPRequest(ServicesSdpRequest $request)
    {
        try {

            $payload = json_decode($request->get('json'), true);

            if (!isset($payload['data']['id'])) {
                throw new \Exception('Service id not found within payload!' . json_encode($payload));
            }

            $apiServiceId = $payload['data']['id'];

            // transform payload from SDP to be created in MCM
            $filteredPayload = $this->mapArrayKeys($payload['data']);

            // check if the service already exists
            $service = Service::where('sdp_service_id', $apiServiceId)->first();

            if (null == $service) {
                // dispatch new service job and return status ok
                ProcessSDPService::dispatch($filteredPayload, $apiServiceId, 'create')->onQueue('service-event');

                McmLog::info('api', 'Job has been dispatched for new service creation.', [
                    'payload' => $payload['data']
                ]);

                return $this->respondWithSuccess('Payload dispatched for new service: ' . $apiServiceId);
            }

            // As a service was found lets proceed to update the parent and duplicates
            ProcessSDPService::dispatch($filteredPayload, $apiServiceId, 'update')->onQueue('service-event');
            McmLog::info('api', 'Job has been dispatched for service update.', [
                'service_id' => $apiServiceId,
                'payload' => $payload['data']
            ]);

            return $this->respondWithSuccess('Payload dispatched for update of service: ' . $apiServiceId);
        } catch (\Exception $e) {

            $message = sprintf('Error from SDP: %s', $e->getMessage());

            $notifier = new SlackNotifier();
            $notifier->setSlackUsers(env('MCM_SLACK_USERS', ''))
                     ->setSlackIcon(':anguished:')
                     ->setSlackMessage($message)
                     ->sendSlackNotification();

            return $this->respondInternalError($e->getMessage());
        }
    }

    public function mapArrayKeys(array $payload)
    {
        $filteredPayload = [];

        if (isset($payload['attributes'])) {
            foreach ($payload['attributes'] as $key => $item) {
                switch ($key) {
                    case 'country_id':
                        $filteredPayload['country_id'] = $item;
                        break;
                    case 'template_id':
                        $filteredPayload['template_id'] = $item;
                        break;
                    case 'name':
                        $filteredPayload['title'] = $item;
                        $filteredPayload['name'] = strtolower(str_replace(" ", "-", $item));
                        $filteredPayload['display_title'] = $item;
                        break;
                    case 'text_monthly_reminder':
                        $filteredPayload['service_copy'] = $item;
                        break;
                    case 'service_cost':
                        $filteredPayload['service_cost'] = $item;
                        break;
                }
            }
            // manually setting variables to make model happy
            $filteredPayload['country_id'] = 1;
            $filteredPayload['template_id'] = 1;
            $filteredPayload['sdp_service_id'] = $payload['id'];
            $filteredPayload['created_at'] = Carbon::now();
            $filteredPayload['updated_at'] = Carbon::now();
        }

        return $filteredPayload;
    }

}
