<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\ApiController;
use App\Http\Requests\MetricRequest;
use App\Http\Resources\AbstractCollection;
use App\Http\Resources\MetricConditionCollection;
use App\Http\Resources\MetricDimensionCollection;
use App\Models\RuleDimensionType;
use App\Models\RuleMetricType;
use Dingo\Api\Contract\Http\Request;

/**
 * Class MetricController
 *
 * @package App\Http\Api\V1\Controllers
 */
class MetricController extends ApiController
{
    /**
     * @return string
     */
    public function index()
    {
        try {
            $metricAll = RuleMetricType::all();
            $filtered = new AbstractCollection($metricAll);
            
            return $this->respondWithSuccess($filtered);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return MetricConditionCollection|string
     */
    public function getConditions(MetricRequest $request, $metric_type_id)
    {
        try {
            $metric = RuleMetricType::find($metric_type_id);
            $filtered = new MetricConditionCollection($metric->conditionTypes);

            return $this->respondWithSuccess($filtered);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return MetricDimensionCollection|string
     */
    public function getDimensions(MetricRequest $request, $metricTypeId)
    {
        try {
            $metricDimensions = RuleDimensionType::where('metric_type_id', $metricTypeId)->get();
            $filtered = new MetricDimensionCollection($metricDimensions);

            return $this->respondWithSuccess($filtered);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }

    }

    /**
     * @SWG\Get(
     *     path="/v1/metric_types",
     *     tags={"Metric Types"},
     *     operationId="get_all_metric_types",
     *     summary="Get list of all Metric Types",
     *     description="Returns list of all Metric Types",
     *     produces={"application/json"},
     *     schemes={"https"},
     *     @SWG\Parameter(
     *          name="Authorization",
     *          description="Bearer {token}",
     *          required=true,
     *          type="string",
     *          format="Header request",
     *          in="query"
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Returns JSON formatted data object",
     *          @SWG\Schema(ref="#/definitions/RuleMetricType")
     *     ),
     *     @SWG\Response(
     *            response=401,
     *            description="Failed to authenticate because of bad credentials or an invalid authorization header."
     *     ),
     *     @SWG\Response(
     *            response=405,
     *            description="405 Method Not Allowed"
     *     ),
     * )
     */

    /**
     * @SWG\Get(
     *     path="/v1/metric_types/{metric_type_id}/conditions",
     *     tags={"Metric Conditions"},
     *     operationId="get_all_metric_condition_types",
     *     summary="Get list of all specific Condition Metric Types",
     *     description="Returns list of all Condition Metric Types",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="Authorization",
     *          description="Bearer {token}",
     *          required=true,
     *          type="string",
     *          format="Header request",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *         name="Metric Type ID",
     *          description="metric_type_id",
     *          required=true,
     *          type="integer",
     *          format="URL Params",
     *          in="path"
     *     ),
     *     @SWG\Response (
     *          response=200,
     *          description="Returns JSON formatted data object",
     *        @SWG\Schema (
     *          @SWG\Property(
     *              property="id",
     *              type="int",
     *              description="Metric type condition attribute ID",
     *              example="1"
     *          ),
     *          @SWG\Property(
     *              property="name",
     *              type="string",
     *              description="Metric type condition attribute name",
     *              example="does contain"
     *          ),
     *          @SWG\Property(
     *              property="class_name",
     *              type="string",
     *              description="Metric type condition attribute class name",
     *              example="DoesContainConditionType"
     *          ),
     *          @SWG\Property(
     *              property="has_value",
     *              type="int",
     *              description="Metric type condition attribute has value boolean",
     *              example="1"
     *          ),
     *          @SWG\Property(
     *              property="created_at",
     *              type="string",
     *              format="date-time",
     *              description="Metric type condition attribute created date and time",
     *              example="2018-04-09 06:31:07"
     *          ),
     *          @SWG\Property(
     *              property="updated_at",
     *              type="string",
     *              format="date-time",
     *              description="Metric type condition attribute updated date and time",
     *              example="2018-04-09 08:41:12"
     *          ),
     *          @SWG\Property(
     *              property="pivot",
     *              ref="#/definitions/RuleConditionType"
     *          ),
     *        )
     *    ),
     *     @SWG\Response(
     *            response=401,
     *            description="Failed to authenticate because of bad credentials or an invalid authorization header.",
     *        ),
     *     @SWG\Response(
     *            response=405,
     *            description="405 Method Not Allowed",
     *        ),
     *     @SWG\Response(
     *            response=500,
     *            description="Trying to get property 'conditionTypes' of non-object",
     *        ),
     * )
     */

    /**
     * @SWG\Get(
     *     path="/v1/metric_types/{metric_type_id}/dimensions",
     *     tags={"Metric Dimensions"},
     *     operationId="get_all_metric_dimension_types",
     *     summary="Get list of all specific Dimension Metric Types",
     *     description="Returns list of all Dimension Metric Types",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="Authorization",
     *          description="Bearer {token}",
     *          required=true,
     *          type="string",
     *          format="Header request",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *         name="Metric Type ID",
     *          description="metric_type_id",
     *          required=true,
     *          type="integer",
     *          format="URL Params",
     *          in="path"
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Returns JSON formatted Dimension data object of specified Metric ID",
     *          @SWG\Schema(ref="#/definitions/RuleDimensionType")
     *     ),
     *     @SWG\Response(
     *            response=401,
     *            description="Failed to authenticate because of bad credentials or an invalid authorization header.",
     *     ),
     *     @SWG\Response(
     *            response=405,
     *            description="405 Method Not Allowed",
     *     ),
     * )
     */
}
