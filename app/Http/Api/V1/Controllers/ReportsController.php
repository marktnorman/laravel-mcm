<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\ApiController;
use App\Http\Requests\ReportSubscriptionRequest;
use App\Http\Resources\BillingChannelCollection;
use App\Http\Resources\CountryCollection;
use App\Http\Resources\ReportSubscriptionByBannerCollection;
use App\Http\Resources\ReportSubscriptionByBearerCollection;
use App\Http\Resources\ReportSubscriptionByBillingChannelCollection;
use App\Http\Resources\ReportSubscriptionByServiceCollection;
use App\Http\Resources\ReportSubscriptionChartCollection;
use App\Http\Resources\ReportSubscriptionByDateCollection;
use App\Http\Resources\ReportSubscriptionSummaryCollection;
use App\Http\Resources\ServiceCollection;
use App\Models\BillingChannel;
use App\Models\Country;
use App\Models\Service;
use function array_push;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportsController
 * @package App\Http\Api\V1\Controllers
 */
class ReportsController extends ApiController
{
    /**
     * @var string
     */
    protected $redshift;

    /**
     * @var
     */
    protected $user;

    /**
     * ReportsController constructor.
     */
    public function __construct()
    {
        $this->redshift = 'pgsql';
    }

    /**
     * Filters.
     *
     * @return mixed
     */
    public function filters()
    {
        // Billing Channels
        $billingChannels = BillingChannel::orderBy('title')
                            ->get();
        $billingChannels = new BillingChannelCollection($billingChannels);

        // Services
        $services = Service::orderBy('title')
                    ->get();
        $services = new ServiceCollection($services);

        // Countries
        $countries = Country::orderBy('title')
                    ->get();
        $countries = new CountryCollection($countries);

        return $this->respondWithSuccess([
            'billing_channels' => $billingChannels,
            'countries' => $countries,
            'services' => $services
        ]);
    }

    /**
     * Format subscriptions and subscriptions summary request parameters for better performance.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function formatRequestParameters($request)
    {
        // Dates
        // Manipulate date format for better performance
        $request->start_date = $request->start_date . ' 00:00:00';
        $request->end_date = $request->end_date . ' 00:00:00';

        // Set the non-required request parameters to be 'all' if not provided
        if(!isset($request->country_id))
        {
            $request->country_id = 'all';
        }
        if(!isset($request->billing_channel_id))
        {
            $request->billing_channel_id = 'all';
        }
        if(!isset($request->service_id))
        {
            $request->service_id = 'all';
        }

        return $request;
    }

    /**
     * Display a detailed analysis of subscriptions ordered by banner.
     *
     * @param ReportSubscriptionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function subscriptionsByBanner(ReportSubscriptionRequest $request)
    {
        try {
            self::formatRequestParameters($request);

            // Union Query
            // Range Total Query
            $rangeTotalQuery = DB::connection($this->redshift)
                                ->table('aggregation.mcm_global as subscriptions')
                                ->select('banner_id', DB::raw("'range_total' as date"))
                                ->selectRaw('sum(attempted_subscriptions) attempts')
                                ->selectRaw('sum(successful_subscriptions) confirmed')
                                ->selectRaw('sum(impressions) impressions')
                                ->selectRaw('(sum(successful_subscriptions)::float / sum(impressions)::float) as cvr_impressions')
                                ->whereBetween('subscriptions.date', [$request->start_date, $request->end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.service_id', "=", $request->service_id);
            }

            // Order by, Group by
            $rangeTotalQuery->groupBy('banner_id');

            // Banner By Date Query
            $bannerByDateQuery = DB::connection($this->redshift)
                                    ->table('aggregation.mcm_global as subscriptions')
                                    ->selectRaw('banner_id, date::varchar')
                                    ->selectRaw('sum(attempted_subscriptions) attempts')
                                    ->selectRaw('sum(successful_subscriptions) confirmed')
                                    ->selectRaw('sum(impressions) impressions')
                                    ->selectRaw('(sum(successful_subscriptions)::float / sum(impressions)::float) as cvr_impressions')
                                    ->whereBetween('subscriptions.date', [$request->start_date, $request->end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $bannerByDateQuery->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $bannerByDateQuery->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $bannerByDateQuery->where('subscriptions.service_id', "=", $request->service_id);
            }

            // Order by, Group by
            $bannerByDateQuery->groupBy('banner_id')
                                ->groupBy('date');

            // Union
            $subscriptions = $rangeTotalQuery->union($bannerByDateQuery)
                                                ->orderBy('banner_id')
                                                ->orderBy('date')
                                                ->get();

            // Format Data
            $subscriptionsFormatted = [];

            $bannerIds = array_unique(array_column($subscriptions->toArray(), 'banner_id'));

            foreach ($bannerIds as $bannerId) {
                $item = '';
                $itemDates = [];
                foreach ($subscriptions as $subscription) {
                    if ($subscription->banner_id == $bannerId) {
                        if ($subscription->date == 'range_total') {
                            $item = $subscription;
                        } else {
                            array_push($itemDates, $subscription);
                        }
                    }
                }

                $item->dates = collect($itemDates);
                array_push($subscriptionsFormatted, $item);
            }

            $subscriptionsFormatted = collect($subscriptionsFormatted);

            // Transform data
            $subscriptions = new ReportSubscriptionByBannerCollection($subscriptionsFormatted);

            return $this->respondWithSuccess($subscriptions);

        } catch (\Exception $exception) {
            return $this->respondInternalError($exception->getMessage());
        }
    }

    /**
     * Display a detailed analysis of subscriptions ordered by bearer.
     *
     * @param ReportSubscriptionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function subscriptionsByBearer(ReportSubscriptionRequest $request)
    {
        try {
            self::formatRequestParameters($request);

            // Union Query
            // Range Total Query
            $rangeTotalQuery = DB::connection($this->redshift)
                ->table('aggregation.mcm_global as subscriptions')
                ->select('bearer', DB::raw("'range_total' as date"))
                ->selectRaw('sum(attempted_subscriptions) attempts')
                ->selectRaw('sum(successful_subscriptions) confirmed')
                ->selectRaw('sum(impressions) impressions')
                ->selectRaw('(sum(successful_subscriptions)::float / sum(impressions)::float) as cvr_impressions')
                ->whereBetween('subscriptions.date', [$request->start_date, $request->end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.service_id', "=", $request->service_id);
            }

            // Order by, Group by
            $rangeTotalQuery->groupBy('bearer');

            // Banner By Date Query
            $bearerByDate = DB::connection($this->redshift)
                ->table('aggregation.mcm_global as subscriptions')
                ->selectRaw('bearer, date::varchar')
                ->selectRaw('sum(attempted_subscriptions) attempts')
                ->selectRaw('sum(successful_subscriptions) confirmed')
                ->selectRaw('sum(impressions) impressions')
                ->selectRaw('(sum(successful_subscriptions)::float / sum(impressions)::float) as cvr_impressions')
                ->whereBetween('subscriptions.date', [$request->start_date, $request->end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $bearerByDate->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $bearerByDate->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $bearerByDate->where('subscriptions.service_id', "=", $request->service_id);
            }

            // Order by, Group by
            $bearerByDate->groupBy('bearer')
                            ->groupBy('date');

            // Union
            $subscriptions = $rangeTotalQuery->union($bearerByDate)
                            ->orderBy('bearer')
                            ->orderBy('date')
                            ->get();

            // Format Data
            $subscriptionsFormatted = [];

            $bearers = array_unique(array_column($subscriptions->toArray(), 'bearer'));

            foreach ($bearers as $bearer) {
                $item = '';
                $itemDates = [];
                foreach ($subscriptions as $subscription) {
                    if ($subscription->bearer == $bearer) {
                        if ($subscription->date == 'range_total') {
                            $item = $subscription;
                        } else {
                            array_push($itemDates, $subscription);
                        }
                    }
                }

                $item->dates = collect($itemDates);
                array_push($subscriptionsFormatted, $item);
            }

            $subscriptionsFormatted = collect($subscriptionsFormatted);

            // Transform data
            $subscriptions = new ReportSubscriptionByBearerCollection($subscriptionsFormatted);

            return $this->respondWithSuccess($subscriptions);

        } catch (\Exception $exception) {
            return $this->respondInternalError($exception->getMessage());
        }
    }

    /**
     * Display a detailed analysis of subscriptions ordered by billing channel id.
     *
     * @param ReportSubscriptionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function subscriptionsByBillingChannel(ReportSubscriptionRequest $request)
    {
        try {
            self::formatRequestParameters($request);

            // Union Query
            // Range Total Query
            $rangeTotalQuery = DB::connection($this->redshift)
                            ->table('aggregation.mcm_global as subscriptions')
                            ->select('billing_channel_id', 'billing_channel_name', DB::raw("'range_total' as date"))
                            ->selectRaw('sum(attempted_subscriptions) attempts')
                            ->selectRaw('sum(successful_subscriptions) confirmed')
                            ->selectRaw('sum(impressions) impressions')
                            ->selectRaw('(sum(successful_subscriptions)::float / sum(impressions)::float) as cvr_impressions')
                            ->whereBetween('subscriptions.date', [$request->start_date, $request->end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.service_id', "=", $request->service_id);
            }

            // Order by, Group by
            $rangeTotalQuery->groupBy('billing_channel_name')
                            ->groupBy('billing_channel_id');

            // Billing Channel By Date Query
            $billingChannelByDate = DB::connection($this->redshift)
                ->table('aggregation.mcm_global as subscriptions')
                ->selectRaw('billing_channel_id, billing_channel_name, date::varchar')
                ->selectRaw('sum(attempted_subscriptions) attempts')
                ->selectRaw('sum(successful_subscriptions) confirmed')
                ->selectRaw('sum(impressions) impressions')
                ->selectRaw('(sum(successful_subscriptions)::float / sum(impressions)::float) as cvr_impressions')
                ->whereBetween('subscriptions.date', [$request->start_date, $request->end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $billingChannelByDate->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $billingChannelByDate->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $billingChannelByDate->where('subscriptions.service_id', "=", $request->service_id);
            }

            // Order by, Group by
            $billingChannelByDate->groupBy('billing_channel_name')
                                    ->groupBy('billing_channel_id')
                                    ->groupBy('date');

            // Union
            $subscriptions = $rangeTotalQuery->union($billingChannelByDate)
                                                ->orderBy('billing_channel_name')
                                                ->get();

            // Format Data
            $subscriptionsFormatted = [];

            $billingChannelIds = array_unique(array_column($subscriptions->toArray(), 'billing_channel_id'));

            foreach ($billingChannelIds as $billingChannelId) {
                $item = '';
                $itemDates = [];
                foreach ($subscriptions as $subscription) {
                    if ($subscription->billing_channel_id == $billingChannelId) {
                        if ($subscription->date == 'range_total') {
                            $item = $subscription;
                        } else {
                            array_push($itemDates, $subscription);
                        }
                    }
                }

                $item->dates = collect($itemDates);
                array_push($subscriptionsFormatted, $item);
            }

            $subscriptionsFormatted = collect($subscriptionsFormatted);

            // Transform data
            $subscriptions = new ReportSubscriptionByBillingChannelCollection($subscriptionsFormatted);

            return $this->respondWithSuccess($subscriptions);

        } catch (\Exception $exception) {
            return $this->respondInternalError($exception->getMessage());
        }
    }

    /**
     * Display a detailed analysis of subscriptions ordered by date.
     *
     * @param ReportSubscriptionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function subscriptionsByDate(ReportSubscriptionRequest $request)
    {
        try {
            self::formatRequestParameters($request);

            // Query
            $subscriptionsQuery = DB::connection($this->redshift)
                ->table('aggregation.mcm_global as subscriptions')
                ->select('date')
                ->selectRaw('sum(attempted_subscriptions) attempts')
                ->selectRaw('sum(successful_subscriptions) confirmed')
                ->selectRaw('sum(impressions) impressions')
                ->selectRaw('(sum(successful_subscriptions)::float / sum(impressions)::float) as cvr_impressions')
                ->whereBetween('subscriptions.date', [$request->start_date, $request->end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $subscriptionsQuery->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $subscriptionsQuery->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $subscriptionsQuery->where('subscriptions.service_id', "=", $request->service_id);
            }

            // Order by, Group by
            $subscriptionsQuery->groupBy('date')
                ->orderBy('date');
            $subscriptions = $subscriptionsQuery->get();

            // Transform data
            $subscriptions = new ReportSubscriptionByDateCollection($subscriptions);
            $subscriptionsChart = new ReportSubscriptionChartCollection($subscriptions);

            return $this->respondWithSuccess([
                'subscriptions' => $subscriptions,
                'subscriptions_chart' => $subscriptionsChart
            ]);

        } catch (\Exception $exception) {
            return $this->respondInternalError($exception->getMessage());
        }
    }

    /**
     * Display a detailed analysis of subscriptions ordered by service.
     *
     * @param ReportSubscriptionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function subscriptionsByService(ReportSubscriptionRequest $request)
    {
        try {
            self::formatRequestParameters($request);

            // Union Query
            // Range Total Query
            $rangeTotalQuery = DB::connection($this->redshift)
                ->table('aggregation.mcm_global as subscriptions')
                ->select('service_id', 'service_name', DB::raw("'range_total' as date"))
                ->selectRaw('sum(attempted_subscriptions) attempts')
                ->selectRaw('sum(successful_subscriptions) confirmed')
                ->selectRaw('sum(impressions) impressions')
                ->selectRaw('(sum(successful_subscriptions)::float / sum(impressions)::float) as cvr_impressions')
                ->whereBetween('subscriptions.date', [$request->start_date, $request->end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $rangeTotalQuery->where('subscriptions.service_id', "=", $request->service_id);
            }

            // Order by, Group by
            $rangeTotalQuery->groupBy('service_id')
                            ->groupBy('service_name');

            // Query
            $serviceByDateQuery = DB::connection($this->redshift)
                ->table('aggregation.mcm_global as subscriptions')
                ->selectRaw('service_id, service_name, date::varchar')
                ->selectRaw('sum(attempted_subscriptions) attempts')
                ->selectRaw('sum(successful_subscriptions) confirmed')
                ->selectRaw('sum(impressions) impressions')
                ->selectRaw('(sum(successful_subscriptions)::float / sum(impressions)::float) as cvr_impressions')
                ->whereBetween('subscriptions.date', [$request->start_date, $request->end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $serviceByDateQuery->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $serviceByDateQuery->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $serviceByDateQuery->where('subscriptions.service_id', "=", $request->service_id);
            }

            // Order by, Group by
            $serviceByDateQuery->groupBy('service_name')
                                ->groupBy('service_id')
                                ->groupBy('date');

            // Union
            $subscriptions = $rangeTotalQuery->union($serviceByDateQuery)
                                                ->orderBy('service_name')
                                                ->get();

            // Format Data
            $subscriptionsFormatted = [];

            $serviceIds = array_unique(array_column($subscriptions->toArray(), 'service_id'));

            foreach ($serviceIds as $serviceId) {
                $item = '';
                $itemDates = [];
                foreach ($subscriptions as $subscription) {
                    if ($subscription->service_id == $serviceId) {
                        if ($subscription->date == 'range_total') {
                            $item = $subscription;
                        } else {
                            array_push($itemDates, $subscription);
                        }
                    }
                }

                $item->dates = collect($itemDates);
                array_push($subscriptionsFormatted, $item);
            }

            $subscriptionsFormatted = collect($subscriptionsFormatted);

            // Transform data
            $subscriptions = new ReportSubscriptionByServiceCollection($subscriptionsFormatted);

            return $this->respondWithSuccess($subscriptions);

        } catch (\Exception $exception) {
            return $this->respondInternalError($exception->getMessage());
        }
    }

    /**
     * Display a summary of subscriptions.
     *
     * @param ReportSubscriptionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function subscriptionsSummary(ReportSubscriptionRequest $request)
    {
        try {
            self::formatRequestParameters($request);

            // Define date ranges depending on how many days there are in the current range
            $daysInRange = Carbon::parse($request->start_date)->diffInDays($request->end_date);
            $request->previous_start_date = Carbon::parse($request->start_date)->subDays($daysInRange);
            $request->previous_end_date = Carbon::parse($request->end_date)->subDays($daysInRange);

            // Current Range Query
            $currentRange = DB::connection($this->redshift)
                            ->table('aggregation.mcm_global as subscriptions')
                            ->select(DB::raw("'current' as range"))
                            ->selectRaw('sum(attempted_subscriptions) attempts')
                            ->selectRaw('sum(successful_subscriptions) confirmed')
                            ->whereBetween('subscriptions.date', [$request->start_date, $request->end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $currentRange->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $currentRange->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $currentRange->where('subscriptions.service_id', "=", $request->service_id);
            }

            // Previous Range Query
            $previousRange = DB::connection($this->redshift)
                            ->table('aggregation.mcm_global as subscriptions')
                            ->select(DB::raw("'previous' as range"))
                            ->selectRaw('sum(attempted_subscriptions) attempts')
                            ->selectRaw('sum(successful_subscriptions) confirmed')
                            ->whereBetween('subscriptions.date', [$request->previous_start_date, $request->previous_end_date]);

            // Additional Conditions
            if($request->billing_channel_id !== 'all') {
                $previousRange->where('subscriptions.billing_channel_id', "=", $request->billing_channel_id);
            }

            if($request->country_id !== 'all') {
                $previousRange->where('subscriptions.country_id', "=", $request->country_id);
            }

            if($request->service_id !== 'all') {
                $previousRange->where('subscriptions.service_id', "=", $request->service_id);
            }

            $ranges = $currentRange->union($previousRange)
                                    ->get();

            // Format Data
            $attemptsCurrent = '';
            $attemptsPrevious= '';
            $attemptsIncrease = false;

            $confirmedCurrent = '';
            $confirmedPrevious = '';
            $confirmedIncrease = false;

            foreach ($ranges as $range) {
                if ($range->range == 'previous') {
                    $attemptsPrevious = $range->attempts;
                    $confirmedPrevious = $range->confirmed;
                } else {
                    $attemptsCurrent = $range->attempts;
                    $confirmedCurrent = $range->confirmed;
                }
            }

            if ($attemptsCurrent > $attemptsPrevious) {
                $attemptsIncrease = true;
            }

            if ($confirmedCurrent > $confirmedPrevious) {
                $confirmedIncrease = true;
            }

            // Transform data
            $subscriptionsSummary = collect([
                (object) [
                    'title' => 'Attempts',
                    'value' => $attemptsCurrent,
                    'increase' => $attemptsIncrease
                ],
                (object) [
                    'title' => 'Confirmed Subscriptions',
                    'value' => $confirmedCurrent,
                    'increase' => $confirmedIncrease
                ]
            ]);

            return $this->respondWithSuccess(new ReportSubscriptionSummaryCollection($subscriptionsSummary));

        } catch (\Exception $exception) {
            return $this->respondInternalError($exception->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/v1/reports_filters",
     *     operationId="get_reports_filters",
     *     tags={"Reports"},
     *     summary="Get reports filters",
     *     description="Get reports filters",
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *  )
     *
     */

    /**
     * @SWG\Get(
     *     path="/v1/reports_subscriptions_by_banner",
     *     operationId="get_reports_subscriptions_by_banner",
     *     tags={"Reports"},
     *     summary="Get reports subscriptions by banner",
     *     description="Get reports subscriptions by banner",
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         type="string",
     *         description="Start date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         type="string",
     *         description="End date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="country_id",
     *         in="query",
     *         type="string",
     *         description="Country Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="billing_channel_id",
     *         in="query",
     *         type="string",
     *         description="Billing Channel Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="service_id",
     *         in="query",
     *         type="string",
     *         description="Service Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *  )
     *
     */

    /**
     * @SWG\Get(
     *     path="/v1/reports_subscriptions_by_bearer",
     *     operationId="get_reports_subscriptions_by_bearer",
     *     tags={"Reports"},
     *     summary="Get reports subscriptions by bearer",
     *     description="Get reports subscriptions by bearer",
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         type="string",
     *         description="Start date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         type="string",
     *         description="End date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="country_id",
     *         in="query",
     *         type="string",
     *         description="Country Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="billing_channel_id",
     *         in="query",
     *         type="string",
     *         description="Billing Channel Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="service_id",
     *         in="query",
     *         type="string",
     *         description="Service Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *  )
     *
     */

    /**
     * @SWG\Get(
     *     path="/v1/reports_subscriptions_by_billing_channel",
     *     operationId="get_reports_subscriptions_by_billing_channel",
     *     tags={"Reports"},
     *     summary="Get reports subscriptions by billing channel",
     *     description="Get reports subscriptions by billing channel",
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         type="string",
     *         description="Start date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         type="string",
     *         description="End date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="country_id",
     *         in="query",
     *         type="string",
     *         description="Country Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="billing_channel_id",
     *         in="query",
     *         type="string",
     *         description="Billing Channel Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="service_id",
     *         in="query",
     *         type="string",
     *         description="Service Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *  )
     *
     */

    /**
     * @SWG\Get(
     *     path="/v1/reports_subscriptions_by_date",
     *     operationId="get_reports_subscriptions_by_date",
     *     tags={"Reports"},
     *     summary="Get reports subscriptions by date",
     *     description="Get reports subscriptions by date",
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         type="string",
     *         description="Start date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         type="string",
     *         description="End date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="country_id",
     *         in="query",
     *         type="string",
     *         description="Country Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="billing_channel_id",
     *         in="query",
     *         type="string",
     *         description="Billing Channel Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="service_id",
     *         in="query",
     *         type="string",
     *         description="Service Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *  )
     *
     */

    /**
     * @SWG\Get(
     *     path="/v1/reports_subscriptions_by_service",
     *     operationId="get_reports_subscriptions_by_service",
     *     tags={"Reports"},
     *     summary="Get reports subscriptions by service",
     *     description="Get reports subscriptions by service",
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         type="string",
     *         description="Start date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         type="string",
     *         description="End date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="country_id",
     *         in="query",
     *         type="string",
     *         description="Country Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="billing_channel_id",
     *         in="query",
     *         type="string",
     *         description="Billing Channel Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="service_id",
     *         in="query",
     *         type="string",
     *         description="Service Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *  )
     *
     */

    /**
     * @SWG\Get(
     *     path="/v1/reports_subscriptions_summary",
     *     operationId="get_reports_subscriptions_summary",
     *     tags={"Reports"},
     *     summary="Get reports subscriptions summary",
     *     description="Get reports subscriptions summary",
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         type="string",
     *         description="Start date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         type="string",
     *         description="End date to query with",
     *         required=true,
     *      ),
     *     @SWG\Parameter(
     *         name="country_id",
     *         in="query",
     *         type="string",
     *         description="Country Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="billing_channel_id",
     *         in="query",
     *         type="string",
     *         description="Billing Channel Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Parameter(
     *         name="service_id",
     *         in="query",
     *         type="string",
     *         description="Service Id to query with",
     *         required=false,
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *  )
     *
     */
}
