<?php
/**
 * Created by PhpStorm.
 * User: kylepretorius
 * Date: 22/02/2018
 * Time: 09:15
 */

namespace App\Http\Api\V1\Controllers;

use App\Http\V1\Requests\SignUpRequest;
use Illuminate\Support\Facades\Config;
use App\Models\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignupController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        $user = new User($request->all());
        if (!$user->save()) {
            throw new HttpException(500);
        }

        if (!Config::get('boilerplate.sign_up.release_token')) {
            return response()->json([
                'status' => 'ok',
            ], 201);
        }

        $token = $JWTAuth->fromUser($user);

        return response()->json([
            'status' => 'ok',
            'token'  => $token,
        ], 201);
    }
}