<?php

namespace App\Http\V1\Requests;

use Dingo\Api\Http\FormRequest;

class ServicesSdpRequest extends FormRequest
{
    public function rules()
    {
        return [
            //'service_id' => 'required|numeric|min:1',
            'json' => 'required|array',
            'source' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    protected function validationData()
    {
        return array_merge($this->request->all(), [
            //'service_id' => $this->route('service_id'),
            'json' => json_decode($this->get('json'),true),
            'source' => $this->get('source'),
        ]);
    }
}
