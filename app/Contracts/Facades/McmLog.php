<?php

namespace App\Contracts\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Illuminate\Log\Writer
 */
class McmLog extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * USAGE -
     *
     * use App\Contracts\Facades\McmLog;
     * McmLog::error('service', 'We dont have a service');
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'mcmlog';
    }
}