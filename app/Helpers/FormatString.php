<?php

namespace App\Helpers;

use NumberFormatter;

class FormatString
{
    /**
     * Format string number for better readability, i.e. 8000 to 8 000.
     *
     * @param $string
     * @param $decimal
     * @return string
     */
    public static function numberReadable($string, $decimal) {
        return (number_format($string, $decimal, ',', ' '));
    }

    /**
     * Convert string number to percentage.
     *
     * @param $string
     * @return string
     */
    public static function numberPercentage($string) {
        $number = round((float)$string * 100, 2 );
        return "{$number}%";
    }

    /**
     * Convert string number to currency.
     *
     * @param $string
     * @return string
     */
    public static function numberCurrency($string) {
        $numberFormatter = new NumberFormatter( 'en_ZA', NumberFormatter::CURRENCY );
        return $numberFormatter->formatCurrency((float)$string, 'ZAR');
    }

}