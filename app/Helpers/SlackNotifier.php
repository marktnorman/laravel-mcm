<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;


class SlackNotifier
{

    private $slackUrl;
    private $slackUsername;
    private $slackChannel;
    private $slackUsers;
    private $slackIcon;
    private $slackMessage;


    public function __construct($slackUrl='https://hooks.slack.com/services/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', $slackUsername='MCM', $slackChannel='#mcm-alerts')
    {
        $this->slackUrl = $slackUrl;
        $this->slackUsername = $slackUsername;
        $this->slackChannel = $slackChannel;
    }


    /**
     * @return mixed
     */
    public function getSlackMessage()
    {
        return $this->slackMessage;
    }

    /**
     * @param mixed $message
     *
     * @return $this
     */
    public function setSlackMessage($message)
    {
        $this->slackMessage = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlackUsers()
    {
        return $this->slackUsers;
    }

    /**
     * @param $slackUsers
     * @return $this
     */
    public function setSlackUsers($slackUsers)
    {
        $this->slackUsers = str_replace([",","[","]"],"",$slackUsers);

        return $this;
    }

    /**
     * @return string
     */
    public function getSlackIcon()
    {
        return $this->slackIcon;
    }

    /**
     * @param $slackUsers
     * @return $this
     */
    public function setSlackIcon($slackIcon)
    {
        $this->slackIcon = $slackIcon;

        return $this;
    }

    public function sendSlackNotification()
    {

        try {
            $client = new \Maknz\Slack\Client($this->slackUrl, [
                'username' => $this->slackUsername,
                'channel' => $this->slackChannel,
                'link_names' => true,
            ]);

            $client->send(sprintf('NB: %s' . PHP_EOL . '%s %s'
            , $this->slackUsers, $this->slackIcon, $this->slackMessage));

        } catch (\Exception $e) {
            Log::debug('Error found with Slack in notifier!: ' . $e->getMessage());
        }

    }



}