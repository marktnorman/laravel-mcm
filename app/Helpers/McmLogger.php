<?php

namespace App\Helpers;

use Monolog\Logger;
use Monolog\Formatter\LineFormatter;

class McmLogger
{
    /**
     * @var array
     */
    protected $channels;

    /**
     * @var array
     * @description Protected log levels
     */
    protected $levels = [
        'debug'     => Logger::DEBUG,
        'info'      => Logger::INFO,
        'notice'    => Logger::NOTICE,
        'warning'   => Logger::WARNING,
        'error'     => Logger::ERROR,
        'critical'  => Logger::CRITICAL,
        'alert'     => Logger::ALERT,
        'emergency' => Logger::EMERGENCY,
    ];

    /**
     * McmLogger constructor.
     *
     * @description Setting the channels to array of channels
     */
    public function __construct()
    {
        $this->channels = [
            'api'            => [
                'path'  => 'logs/api.log',
                'level' => env("APP_LOG_LEVEL", Logger::INFO),
            ],
            'flow'            => [
                'path'  => 'logs/flow.log',
                'level' => env("APP_LOG_LEVEL", Logger::INFO),
            ],
            'event'             => [
                'path'  => 'logs/event.log',
                'level' => env("APP_LOG_LEVEL", Logger::INFO),
            ],
            'laravel'           => [
                'path'  => 'logs/laravel.log',
                'level' => env("APP_LOG_LEVEL", Logger::INFO),
            ],
        ];
    }

    /**
     * @param       $channel
     * @param       $level
     * @param array $params
     */
    public function writeLog($channel, $level, $message, array $context = [])
    {
        // Check if specified channel exist
        if (!in_array($channel, array_keys($this->channels))) {
            throw new InvalidArgumentException('Invalid channel used.');
        }

        // Lazy load logger
        if (!isset($this->channels[$channel]['_instance'])) {
            // Create instance
            $this->channels[$channel]['_instance'] = new Logger($channel);
            // Add custom handler
            $this->channels[$channel]['_instance']->pushHandler(
                (new McmStreamHandler(
                    $channel,
                    storage_path() . '/' . $this->channels[$channel]['path'],
                    $this->channels[$channel]['level']
                ))->setFormatter(new LineFormatter(null, null, true, true))
            );
        }

        // Check if message is set, if so add key message and value to context variable
        if (!empty($message)) {
            $context['message'] = $message;
        }

        // Check if $_REQUEST object has been attached, if so, lets log it for context
        if (isset($_REQUEST['detectionData'])) {
            $context['detectionData'] = $_REQUEST['detectionData'];
        }

        // Write out record
        $this->channels[$channel]['_instance']->{$level}($message, $context);
    }

    /**
     * @param       $channel
     * @param array $params
     */
    public function write($channel, $message, array $context = [])
    {
        // Get method name for the associated level
        $level = array_flip($this->levels)[$this->channels[$channel]['level']];

        $this->writeLog($channel, $level, $message, $context);
    }

    /**
     * @param $func
     * @param $params
     */
    function __call($func, $params)
    {
        if (in_array($func, array_keys($this->levels))) {
            if (isset($params[2])) {
                return $this->writeLog($params[0], $func, $params[1], $params[2]);
            } else {
                return $this->writeLog($params[0], $func, $params[1]);
            }
        }
    }
}