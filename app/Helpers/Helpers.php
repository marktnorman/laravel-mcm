<?php

namespace App\Helpers;

class Helpers
{
    /**
     * Helper function that recursively searches an object looking for
     * instances of a slug and returning it's value when found.
     *
     * @param $object
     * @param $key
     * @param $value
     */
    public static function arrayKeySearch($object, $key, &$value)
    {
        $array = (array)$object;

        // If our key is found then set it
        if (isset($array[$key])) {
            $value = $array[$key];

            if (isset($value) && !empty($value)) {
                return;
            }
        }

        // If there is an index with a value of zero then we have converted an object with no sub-objects
        if (isset($array['0'])) {
            return;
        }

        // Loop through the object
        foreach ($object as $subObject) {
            self::arrayKeySearch($subObject, $key, $value);
        }
    }
}

