<?php

namespace App\Jobs;

use App\Models\Service;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use DB;

class ProcessSDPService implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Maximum attempts before stopping
     */
    public $tries = 3;

    /**
     * The number of seconds the job can run before timing out.
     */
    public $timeout = 10;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $payload;
    private $apiServiceId;
    private $processType;

    public function __construct(array $payload, $apiServiceId, $processType = 'create')
    {
        $this->payload = $payload;
        $this->apiServiceId = $apiServiceId;
        $this->processType = $processType;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** Event is handled via CLI - no need for event */

        Log::info('Processing payload in queue');

        try {

            if(empty($this->payload))
            {
                throw new \Exception('Payload cannot be empty!');
            }

            if($this->processType == 'create')
            {
                try {
                    // Saving the content with the filtered array
                    DB::table('services')->insert($this->payload);
                    Log::info('Service created for sdp service id: ' . $this->apiServiceId);
                } catch(\Exception $e) {
                    throw new \Exception('Error with Insert: ' . $e->getMessage());
                }
            } else {

                $mcmService = Service::where('sdp_service_id', $this->apiServiceId)->pluck('id');
                //iterate over each service to make sure they all updated.
                foreach($mcmService as $value)
                {
                    $service = Service::find($value);
                    $service->fill($this->payload)->save();
                }
                Log::info('All services updated for sdp service id: ' . $this->apiServiceId);

            }

        } catch (\Exception $e) {
            Log::error("Error: " . $e->getMessage());
            throw new \Exception("Error: " . $e->getMessage());
        }
    }

    public function failed(\Exception $exception)
    {
        Log::error($exception->getMessage());
    }


}
