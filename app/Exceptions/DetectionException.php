<?php

namespace App\Exceptions;

use Exception;

class DetectionException extends Exception
{
    // Silence is golden
}
