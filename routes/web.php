<?php

/**
 * Required set routes
 */
Route::group(['middleware' => ['detection', 'rule_engine']], function () {
    Route::get('/service/{network_name}/{service_name}', "ServicesController@handle");
    Route::get('{network_name}/service/{service_name}', "ServicesController@handle");
    Route::get('/s/{service_id}', "ServicesController@handle");
    Route::get('/service/{service_id}', "ServicesController@handle")->where('service_id', '[0-9]+');
    Route::get('/service/{service_name}', "ServicesController@handle");
    Route::get('/club/{service_name}', "ServicesController@handle");
    Route::get('/service/{service_id}', "ServicesController@handle")->where('service_id', '[0-9]+');
});