<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */

$api = app(Router::class);

$api->version('v1', function (Router $api) {

    /*
    |--------------------------------------------------------------------------
    | Authentication routes http://{domain}/api/auth/
    |--------------------------------------------------------------------------
    | Routes that are used for authentication
    */
    $api->post('sign_up', 'App\Http\Api\V1\Controllers\SignupController@signUp');

    $api->group(['prefix' => 'auth'], function(Router $api) {

        // Authentication Routes
        // Logs a user in
        $api->post('login', 'App\Http\Api\Auth\AuthController@login');

    });


    // Middleware that handles payloads from SDPD requests
    $api->group(['middleware' => ['authpayloadrequest'], 'prefix' => 'payload', 'namespace' => 'App\Http\Api\V1\Controllers'], function(Router $api)
    {
        $api->post('service_sync','ServiceApiController@processSDPRequest');
    });

    /*
    |--------------------------------------------------------------------------
    | Version 1 http://{domain}/api/v1/
    |--------------------------------------------------------------------------
    | Routes that are protected via authentication
    */
    $api->group(['middleware' => ['api.auth'], 'prefix' => 'v1', 'namespace' => 'App\Http\Api\V1\Controllers'], function(Router $api)
    {
        // Metrics
        $api->get('metric_types', 'MetricController@index');
        $api->get('metric_types/{metric_type_id}/conditions', 'MetricController@getConditions');
        $api->get('metric_types/{metric_type_id}/dimensions', 'MetricController@getDimensions');

        // Reports
        $api->get('reports_filters', 'ReportsController@filters');
        $api->get('reports_subscriptions_by_banner', 'ReportsController@subscriptionsByBanner');
        $api->get('reports_subscriptions_by_bearer', 'ReportsController@subscriptionsByBearer');
        $api->get('reports_subscriptions_by_billing_channel', 'ReportsController@subscriptionsByBillingChannel');
        $api->get('reports_subscriptions_by_date', 'ReportsController@subscriptionsByDate');
        $api->get('reports_subscriptions_by_service', 'ReportsController@subscriptionsByService');
        $api->get('reports_subscriptions_summary', 'ReportsController@subscriptionsSummary');

        // Services
        $api->post('service/{service_id}','ServiceApiController@processSDPRequest');

    });
});
