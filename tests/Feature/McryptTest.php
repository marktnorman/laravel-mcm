<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class McryptTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDecrypt()
    {
        // Set the Config
        $cipherKey = "197e8cef347261a41f6c3c29e1110e63ef2e0d055b2e77b4a13bae38b4416c09"; //stays the same on both sides
        $msisdn = "27738196139";
        $payload = "hXanr17+zL/jyA3q39FzqA==";


        $encrypt = (new \JaegerApp\Encrypt)->setKey($cipherKey);

        $decoded = $encrypt->decode($payload);

        $this->assertTrue($decoded == $msisdn);
    }
}
