<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Network;
use App\Models\Service;
use Closure;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Exceptions\DetectionException;
use Mcm\McmDetection\HyveMnoDetect;
use KylePretorius\IPChecker\IPChecker;

class DetectionTest extends TestCase
{
    /**
     * @var ndpUrl
     */
    public $ndpUrl;
    /**
     * @var npdToken
     */
    public $npdToken;
    /**
     * @var requestData
     */
    public $requestData;
    /**
     * @var countryIsoCode
     */
    public $countryIsoCode;
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * DetectionMiddleware constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
        $this->ndpUrl = env('NPD_URL', 'npd.hyvesdp.com/api/v2/network');
        $this->npdToken = env('NPD_TOKEN', '3106a37e08ddb39c69f7c8d17f049b01707c3a66fb0aa8d5e4d12dcc66af3a18');
    }

    /**
     * @param          $request
     * @param \Closure $next
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        // Uncomment these values for mimicking request information
        // $_SERVER['HTTP_Y_MSISDN'] = '27641098637';
        // $_SERVER['HTTP_Y_MSISDN'] = '27609918373'; // Vodacom msisdn
        // $_SERVER['HTTP_X_MSISDN'] = '27767509890';
        // $_REQUEST['mcrypt_token'] = '753bfc8cf7de2340bf9e28ea8805e77';
        // $_REQUEST['mcrypt_data'] = 'MLiAc0Fi/QRlklWpxhqzUg==';
        // $_SERVER['HTTP_FORWARDED'] = '100.88.0.11'; // Vodacom IP
        // $_SERVER['HTTP_FORWARDED'] = '41.48.0.18'; // Cellc IP

        // Check if service ID is set, if not lets check if service name is set in order to retrieve the service
        if (null !== $request->route('service_id')) {
            $service = Service::find($request->route('service_id'));
        } else if (null !== $request->route('service_name')) {
            $service = Service::where('name', $request->route('service_name'))->first();
        }

        if (null === $service) {
            McmLog::warning('flow', 'Service set in route does not match any internal stored service.', [
                'request_url' => $request->url(),
            ]);

            throw new DetectionException('No service has been found');
        }

        // We detected we have a service so set the country iso code
        if (null !== $service) {
            $this->countryIsoCode = $service->country()->value('country_iso_code');
        }

        // Attach to request the route specified network_name if set run detection from config
        $this->requestData = $this->detectionConfig();

        // Attach to request the route specified network_name if set
        if ($request->route('network_name') !== null) {
            $this->requestData['msisdnNetworkData']['route_detected_network'] = $request->route('network_name');
        }

        // Service by now should be found, so lets add to request data
        if (isset($service->id)) {
            $this->requestData['serviceData']['service_id'] = $service->id;
        }

        // Get the MNO IP range
        $this->ipNetworkDetect();

        // NPD api call
        $this->npdApiDetect();

        // Check if Mcrypt needs to be utilised
        $this->mcryptDetection();

        // Attach to the request
        $request->attributes->add(['detectionData' => (Object)$this->requestData]);
        $request->attributes->add(['countryIsoCode' => $this->countryIsoCode]);

        //Attach requestData to the $_REQUEST object
        $_REQUEST['detectionData'] = (Object)$this->requestData;

        return $next($request);
    }

    /**
     * Query NPD for existing msisdns
     */
    protected function npdApiDetect()
    {
        foreach ($this->requestData['headerEnrichmentData'] as $key => $msisdn) {
            $this->requestData['headerEnrichmentData'][$key] = $this->npdApiRequest($msisdn);
        }

        foreach ($this->requestData['msisdnUrlData'] as $key => $msisdn) {
            $this->requestData['msisdnUrlData'][$key] = $this->npdApiRequest($msisdn);
        }

        if (isset($this->requestData['msisdnUrlData']['msisdn']->npd->data->attributes->slug) && !empty($this->requestData['msisdnUrlData']['msisdn']->npd->data->attributes->slug)) {
            $this->requestData['msisdnNetworkData']['npd_detected_network'] = $this->requestData['msisdnUrlData']['msisdn']->npd->data->attributes->slug;
        }
    }

    /**
     * @param int $msisdn
     *
     * @return object
     */
    protected function npdApiRequest($msisdn = 0)
    {
        try {
            $npdRequest = $this->client->get($this->ndpUrl, [
                'query'   => [
                    'msisdn'  => $msisdn,
                    'country' => strtoupper($this->countryIsoCode),
                ],
                'headers' => [
                    'Authorization' => 'Token ' . $this->npdToken,
                    'Accept'        => 'application/json',
                ],
            ]);

            return (Object)['msisdn' => $msisdn, 'npd' => json_decode($npdRequest->getBody()->getContents())];

        } catch (ClientException $e) {
            McmLog::error('flow', 'Error found with NPD request.', [
                'message_context' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Check if Mcrypt is set in request, if so decode it and add msisdn to request object
     */
    protected function mcryptDetection()
    {
        if (isset($this->requestData['mcryptUrlData']['mcrypt_token']) && isset($this->requestData['mcryptUrlData']['mcrypt_data'])) {
            $encrypt = new \JaegerApp\Encrypt;
            $cipherKey = Network::where('slingshot_token', $this->requestData['mcryptUrlData']['mcrypt_token'])->value('slingshot_cypher_key');
            if (null !== $cipherKey) {
                $encrypt->setKey($cipherKey);
                $decoded = $encrypt->decode($this->requestData['mcryptUrlData']['mcrypt_data']);
                $this->requestData['mcryptQueryVarKeys']['mcrypt_msisdn'] = $this->npdApiRequest($decoded);
                // Set our network slug retrieved from the NPD request by use of Mcrypt
                if (isset($this->requestData['mcryptQueryVarKeys']['mcrypt_msisdn']->npd->data->attributes->slug) && !empty($this->requestData['mcryptQueryVarKeys']['mcrypt_msisdn']->npd->data->attributes->slug)) {
                    $this->requestData['msisdnNetworkData']['mcrypt_npd_detected_network'] = $this->requestData['mcryptQueryVarKeys']['mcrypt_msisdn']->npd->data->attributes->slug;
                }
            }
        }
    }

    /**
     * @throws \Exception
     */
    protected function ipNetworkDetect()
    {
        $networks = Network::whereNotNull('ip_range')->whereRaw('ip_range <> ""')->get();
        foreach ($networks as $network) {
            $ipAddressData = array_values($this->requestData['ipAddressData']);
            $networkName = $network->getAttribute('name');
            $ipChecker = new IPChecker(json_decode($network->getAttribute('ip_range')));
            foreach ($ipAddressData as $row) {
                $ipMatchedUp = $ipChecker->check($row);
                if ($ipMatchedUp) {
                    $this->requestData['msisdnNetworkData']['ip_detected_network'] = $networkName;
                }
            }
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function detectionConfig()
    {
        // Implement detection
        $data = HyveMnoDetect::detect([
            "headerEnrichmentKeys" => [
                'HTTP_MSISDN',
                'HTTP_IMISDN',
                'HTTP_X_MSISDN',
                'HTTP_Y_MSISDN',
                'HTTP_X_NETWORK_INFO',
                'HTTP_X_WAP_MSISDN',
                'HTTP_X_BAM_MSISDN',
                'HTTP_X_NOKIA_BEARER',
                'HTTP_X_NOKIA_GATEWAY_ID',
                'HTTP_X_NOKIA_IPADDRESS',
                'HTTP_X_NOKIA_MSISDN',
                'HTTP_X_UP_CALLING_LINE_ID',
                'HTTP_X_WAP_NETWORK_CLIENT_MSISDN',
                'HTTP_X_MSP_MSISDN',
            ],
            "ipAddressKeys"        => [
                'HTTP_X_FORWARDED_FOR',
                'HTTP_CLIENT_IP',
                'REMOTE_ADDR',
                'HTTP_FORWARDED',
            ],
            "msisdnQueryVarKeys"   => [
                'msisdn',
            ],
            "mcryptQueryVarKeys"   => [
                'mcrypt_data',
                'mcrypt_token',
            ],
        ]);


        return $data;
    }

    /**
     * Generate access token to allow for user tracking
     *
     * @return bool|string
     */
    protected function generate_access_token()
    {
        // Default prefix
        $prefix = 'h_';

        // Opx prefix if set
        if ((isset($_SERVER['HTTP_X_ENRICHMENTSTATUS']))) {
            $prefix = 'opx_';
        }

        // Generate the token with the prefix above
        return $prefix . md5(microtime() . uniqid('', true));
    }
}
