<?php

use Illuminate\Database\Seeder;
use App\Models\Banner;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banner::create([
            'name'       => 'opera_thank_you_version_2',
            'banner_type_id' => 1,
            'format' => 'jpg'
        ]);

        Banner::create([
            'name'       => 'opera_mesh',
            'banner_type_id' => 3,
            'format' => 'jpg'
        ]);

        Banner::create([
            'name'       => 'opera_featured_plus',
            'banner_type_id' => 2,
            'format' => 'jpg'
        ]);

        Banner::create([
            'name'       => 'mtn_featured',
            'banner_type_id' => 3,
            'format' => 'jpg'
        ]);

        Banner::create([
            'name'       => 'mtn_thanks',
            'banner_type_id' => 1,
            'format' => 'jpg'
        ]);
    }
}
