<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email'                => 'kyle@hyvemobile.co.za',
            'first_name'           => 'Kyle',
            'last_name'            => 'Pretorius',
            'password'             => 'test123'
        ]);

        User::create([
            'email'                => 'vince@hyvemobile.co.za',
            'first_name'           => 'Vince',
            'last_name'            => 'Kruger',
            'password'             => 'test123'
        ]);

        User::create([
            'email'                => 'mark@hyvemobile.co.za',
            'first_name'           => 'Mark',
            'last_name'            => 'Norman',
            'password'             => 'test123'
        ]);
    }
}
