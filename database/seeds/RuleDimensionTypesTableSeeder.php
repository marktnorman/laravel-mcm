<?php

use Illuminate\Database\Seeder;
use App\Models\RuleDimensionType;

class RuleDimensionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RuleDimensionType::create([
            'metric_type_id' => 1,
            'name'       => 'ip_address',
            'class_name' => 'IpAddressDimensionType',
        ]);

        RuleDimensionType::create([
            'metric_type_id' => 2,
            'name'       => 'detected_network',
            'class_name' => 'DetectedNetworkDimensionType',
        ]);
    }
}
