<?php

use Illuminate\Database\Seeder;

use App\Models\Service;
use App\Models\BillingChannel;

class BillingChannelServiceRelation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Service::find(1)->billingChannels()->attach(1);
        Service::find(2)->billingChannels()->attach(2);
        Service::find(3)->billingChannels()->attach(2);

    }
}
