<?php

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServiceBannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::find(1)->banner()->attach([1,2,3]);
        Service::find(2)->banner()->attach([4,5]);
    }
}
