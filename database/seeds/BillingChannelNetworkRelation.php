<?php

use Illuminate\Database\Seeder;

use App\Models\BillingChannel;
use App\Models\Network;

class BillingChannelNetworkRelation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Network::find(1)->billingChannels()->attach(1);
        Network::find(2)->billingChannels()->attach(2);
        Network::find(3)->billingChannels()->attach(2);
    }
}
