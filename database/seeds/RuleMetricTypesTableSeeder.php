<?php

use Illuminate\Database\Seeder;
use App\Models\RuleMetricType;

class RuleMetricTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RuleMetricType::create([
            'name'       => 'mno_ip_addresses',
            'class_name' => 'MnoIpAddressMetricType',
            'variable'   => 'mnoIpAddresses',
        ]);

        RuleMetricType::create([
            'name'       => 'msisdn',
            'class_name' => 'MsisdnMetricType',
            'variable'   => 'msisdn',
        ]);

        RuleMetricType::create([
            'name'       => 'detected_network',
            'class_name' => 'DetectedNetworkMetricType',
            'variable'   => 'detectedNetwork',
        ]);

        RuleMetricType::create([
            'name'       => 'billing_channel.internal_doi_allowed',
            'class_name' => 'BillingChannelInternalDoiAllowedMetricType',
            'variable'   => 'channelInternalDoiAllowed',
        ]);

        RuleMetricType::create([
            'name'       => 'service_id',
            'class_name' => 'ServiceIdMetricType',
            'variable'   => 'serviceId',
        ]);

        RuleMetricType::create([
            'name'       => 'billing_channel.forced_wap_doi',
            'class_name' => 'BillingChannelForcedWapDoiMetricType',
            'variable'   => 'channelForcedWapDoi',
        ]);

        RuleMetricType::create([
            'name'       => 'user_agent',
            'class_name' => 'UserAgentMetricType',
            'variable'   => 'userAgent',
        ]);

        RuleMetricType::create([
            'name'       => 'browser_version',
            'class_name' => 'BrowserVersionMetricType',
            'variable'   => 'browserVersion',
        ]);

        RuleMetricType::create([
            'name'       => 'browser',
            'class_name' => 'BrowserMetricType',
            'variable'   => 'browser',
        ]);

        RuleMetricType::create([
            'name'       => 'msisdn_trusted',
            'class_name' => 'MsisdnTrustedMetricType',
            'variable'   => 'msisdnTrusted',
        ]);

        RuleMetricType::create([
            'name'       => 'service_name',
            'class_name' => 'ServiceNameMetricType',
            'variable'   => 'serviceName',
        ]);
    }
}
