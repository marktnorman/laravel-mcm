<?php

use Illuminate\Database\Seeder;
use App\Models\RuleCondition;

class RuleConditionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RuleCondition::create([
            'rule_id'  => 1,
            'operator' => '',
            'metric_type_id' => 3,
            'condition_type_id' => 5,
            'dimension_type_id' => NULL,
            'value' => 'cellc',
        ]);

        RuleCondition::create([
            'rule_id'  => 1,
            'operator' => 'AND',
            'metric_type_id' => 2,
            'condition_type_id' => 7,
            'dimension_type_id' => NULL,
            'value' => '',
        ]);

        RuleCondition::create([
            'rule_id'  => 2,
            'operator' => '',
            'metric_type_id' => 2,
            'condition_type_id' => 8,
            'dimension_type_id' => NULL,
            'value' => '',
        ]);

        RuleCondition::create([
            'rule_id'  => 2,
            'operator' => 'AND',
            'metric_type_id' => 3,
            'condition_type_id' => 5,
            'dimension_type_id' => NULL,
            'value' => 'cellc',
        ]);

        RuleCondition::create([
            'rule_id'  => 2,
            'operator' => 'AND',
            'metric_type_id' => 4,
            'condition_type_id' => 9,
            'dimension_type_id' => NULL,
            'value' => '',
        ]);

        RuleCondition::create([
            'rule_id'  => 3,
            'operator' => '',
            'metric_type_id' => 3,
            'condition_type_id' => 5,
            'dimension_type_id' => NULL,
            'value' => 'mtn-ng-sdp',
        ]);

        RuleCondition::create([
            'rule_id'  => 4,
            'operator' => '',
            'metric_type_id' => 3,
            'condition_type_id' => 3,
            'dimension_type_id' => NULL,
            'value' => '',
        ]);

        RuleCondition::create([
            'rule_id'  => 5,
            'operator' => '',
            'metric_type_id' => 6,
            'condition_type_id' => 9,
            'dimension_type_id' => NULL,
            'value' => '',
        ]);

        RuleCondition::create([
            'rule_id'  => 6,
            'operator' => '',
            'metric_type_id' => 3,
            'condition_type_id' => 5,
            'dimension_type_id' => NULL,
            'value' => 'safaricom',
        ]);

        RuleCondition::create([
            'rule_id'  => 7,
            'operator' => '',
            'metric_type_id' => 1,
            'condition_type_id' => 1,
            'dimension_type_id' => 1,
            'value' => 'vodacom',
        ]);

        RuleCondition::create([
            'rule_id'  => 8,
            'operator' => '',
            'metric_type_id' => 9,
            'condition_type_id' => 5,
            'dimension_type_id' => NULL,
            'value' => 'opera_mini',
        ]);

        RuleCondition::create([
            'rule_id'  => 8,
            'operator' => 'AND',
            'metric_type_id' => 8,
            'condition_type_id' => 11,
            'dimension_type_id' => NULL,
            'value' => '12',
        ]);

        RuleCondition::create([
            'rule_id'  => 8,
            'operator' => 'AND',
            'metric_type_id' => 3,
            'condition_type_id' => 1,
            'dimension_type_id' => NULL,
            'value' => 'vodacom,mtn',
        ]);

        RuleCondition::create([
            'rule_id'  => 9,
            'operator' => '',
            'metric_type_id' => 3,
            'condition_type_id' => 5,
            'dimension_type_id' => 2,
            'value' => 'mtn',
        ]);

        RuleCondition::create([
            'rule_id'  => 9,
            'operator' => 'AND',
            'metric_type_id' => 5,
            'condition_type_id' => 5,
            'dimension_type_id' => NULL,
            'value' => '494',
        ]);

        RuleCondition::create([
            'rule_id'  => 9,
            'operator' => 'AND',
            'metric_type_id' => 10,
            'condition_type_id' => 10,
            'dimension_type_id' => NULL,
            'value' => '',
        ]);
    }
}