<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

/**
 * Class CountriesTableSeeder
 *
 * @author Vince Kruger <vincekruger@gmail.com>
 */
class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create([
            'country_iso_code'     => 'za',
            'country_dialing_code' => '27',
            'name'                 => 'south-africa',
            'title'                => 'South Africa',
        ]);
    }
}
