<?php

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::create([
            'name'       => 'opera-za-jobs-zone',
            'title'      => 'Opera SA - Jobs Zone',
            'sdp_service_id' => 933,
            'template_id' => 1,
            'campaign_keyword' => 'opera-zone',
            'service_cost' => 'R3 / day',
            'service_copy' => 'Jobs Zone costs R3/day. You can cancel your subscription at any time.',
            'display_title' => 'Jobs Zone',
            'post_doi_landing_url' => 'http://jobszone.mobi/redirect/post-doi',
            'forgot_password_url' => 'http://jobszone.mobi/forgot-link',
            'double_optin' => 1,
            'display_interstitial' => 0,
            'country_id' => 1,
            'active'     => 1,
        ]);

        Service::create([
            'name'       => 'vodacom-socca-stars',
            'title'      => 'Vodacom Socca Stars',
            'sdp_service_id' => 135,
            'campaign_keyword' => 'socca-stars',
            'service_cost' => 'R5 / day',
            'service_copy' => 'Socca Stars costs R3/day. You can cancel your subscription at any time.',
            'display_title' => 'Socca Stars',
            'post_doi_landing_url' => 'http://soccastar.mobi/redirect/post-doi',
            'forgot_password_url' => 'http://soccastar.mobi/forgot-link',
            'double_optin' => 1,
            'display_interstitial' => 0,
            'template_id' => 2,
            'active'     => 1,
            'country_id' => 1,
        ]);

        Service::create([
            'name'       => 'mtn-kaizer-chiefs',
            'title'      => 'MTN Kaizer Chiefs',
            'sdp_service_id' => 221,
            'campaign_keyword' => 'kaizer-chiefs',
            'service_cost' => 'R2 / day',
            'service_copy' => 'Kaizer Chiefs costs R3/day. You can cancel your subscription at any time.',
            'display_title' => 'Kaizer Chiefs',
            'post_doi_landing_url' => 'http://kaizerchiefs.mobi/redirect/post-doi',
            'forgot_password_url' => 'http://kaizerchiefs.mobi/forgot-link',
            'double_optin' => 0,
            'display_interstitial' => 0,
            'template_id' => 3,
            'active'     => 1,
            'country_id' => 1,
        ]);


    }
}