<?php

use Illuminate\Database\Seeder;
use App\Models\Rule;

class RuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rule::create([
            'rule_name'  => 'Cell C SlingShot',
            'rule_slug' => 'cell-c-slingshot',
            'level' => 1,
            'template' => '',
            'flow' => 'slingshot',
            'flow_class_name' => 'SlingshotFlow',
            'weight' => 95,
            'active' => 1,
        ]);

        Rule::create([
            'rule_name'  => 'Billing Channel Internal DOI',
            'rule_slug' => 'billing-channel-internal-doi',
            'level' => 1,
            'template' => '',
            'flow' => 'wap_doi',
            'flow_class_name' => 'WapDoiFlow',
            'weight' => 5,
            'active' => 0,
        ]);

        Rule::create([
            'rule_name'  => 'MTN Nigeria',
            'rule_slug' => 'mtn-nigeria',
            'level' => 1,
            'template' => 'thank_you_wap_ussd',
            'flow' => 'wap_ussd',
            'flow_class_name' => 'WapUssdFlow',
            'weight' => 95,
            'active' => 1,
        ]);

        Rule::create([
            'rule_name'  => 'Invalid Network',
            'rule_slug' => 'invalid-network',
            'level' => 1,
            'template' => 'denied',
            'flow' => 'null',
            'flow_class_name' => 'null',
            'weight' => 95,
            'active' => 1,
        ]);

        Rule::create([
            'rule_name'  => 'Forced WAP DOI',
            'rule_slug' => 'foce-wap-doi',
            'level' => 1,
            'template' => 'null',
            'flow' => 'wap_doi',
            'flow_class_name' => 'WapDoiFlow',
            'weight' => 95,
            'active' => 1,
        ]);

        Rule::create([
            'rule_name'  => 'SafariCom Network',
            'rule_slug' => 'safaricom',
            'level' => 1,
            'template' => 'thank_you_wap_ussd',
            'flow' => 'wap_ussd',
            'flow_class_name' => 'WapUssdFlow',
            'weight' => 95,
            'active' => 1,
        ]);

        Rule::create([
            'rule_name'  => 'Vodacom IP Detection',
            'rule_slug' => 'vodacom',
            'level' => 1,
            'template' => 'null',
            'flow' => 'wap_doi',
            'flow_class_name' => 'WapDoiFlow',
            'weight' => 95,
            'active' => 1,
        ]);

        Rule::create([
            'rule_name'  => 'Opera Legacy Version',
            'rule_slug' => 'opera-mini-below-version-12',
            'level' => 1,
            'template' => 'enter_msisdn',
            'flow' => 'wap_sms',
            'flow_class_name' => 'WapSmsFlow',
            'weight' => 95,
            'active' => 1,
        ]);

        Rule::create([
            'rule_name'  => 'GameKings MTN South Africa USSD NI',
            'rule_slug' => 'gamekings-mtn-sa-usdd-ni',
            'level' => 1,
            'template' => 'thank_you_wap_ussd',
            'flow' => 'wap_ussd',
            'flow_class_name' => 'WapUssdFlow',
            'weight' => 95,
            'active' => 1,
        ]);

        Rule::create([
            'rule_name'  => 'Enter Msisdn',
            'rule_slug' => 'enter-msisdn',
            'level' => 1,
            'template' => '',
            'flow' => 'enter-msisdn',
            'flow_class_name' => 'EnterMsisdnFlow',
            'weight' => 100,
            'active' => 1,
        ]);
    }
}
