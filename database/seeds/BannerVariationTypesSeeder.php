<?php

use Illuminate\Database\Seeder;
use App\Models\BannerVariationType;

class BannerVariationTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BannerVariationType::create([
            'name' => 'small',
            'min_width' => 240,
            'max_width' => 320,
            'min_height' => 180,
            'max_height' => 220,
        ]);

        BannerVariationType::create([
            'name' => 'medium',
            'min_width' => 520,
            'max_width' => 720,
            'min_height' => 320,
            'max_height' => 420,
        ]);

        BannerVariationType::create([
            'name' => 'large',
            'min_width' => 720,
            'max_width' => 1024,
            'min_height' => 768,
            'max_height' => 800,
        ]);
    }
}
