<?php

use Illuminate\Database\Seeder;

use App\Models\RuleMetricType;

class ConditionTypeMetricTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        RuleMetricType::find(1)->conditionTypes()->attach(1);
        RuleMetricType::find(2)->conditionTypes()->attach(1);
        RuleMetricType::find(2)->conditionTypes()->attach(2);
        RuleMetricType::find(3)->conditionTypes()->attach(1);
        RuleMetricType::find(3)->conditionTypes()->attach(2);

    }
}
