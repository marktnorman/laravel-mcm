<?php

use Illuminate\Database\Seeder;
use App\Models\BillingChannel;

class BillingChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BillingChannel::create([
            'name'                   => 'cellc',
            'title'                  => 'Cell C',
            'sdp_billing_channel_id' => 1,
            'active'                 => 1,
            'internal_doi'           => 0,
            'forced_wap_doi'         => 0,
            'slingshot_endpoint'     => 'http://cellc.mobi/slingshot/',
            'slingshot_token'        => 'Oh6oo29TUR8081465tq2X5d3O1ln4g',
            'slingshot_cypher_key'   => '65s8547ciH0GQ065aWdp',
        ]);

        BillingChannel::create([
            'name'                   => 'mtn',
            'title'                  => 'MTN',
            'sdp_billing_channel_id' => 4,
            'active'                 => 1,
            'internal_doi'           => 0,
            'forced_wap_doi'         => 0,
            'slingshot_endpoint'     => NULL,
            'slingshot_token'        => NULL,
            'slingshot_cypher_key'   => NULL,
        ]);

        BillingChannel::create([
            'name'                   => 'vodacom',
            'title'                  => 'Vodacom',
            'sdp_billing_channel_id' => 3,
            'active'                 => 1,
            'internal_doi'           => 0,
            'forced_wap_doi'         => 0,
            'slingshot_endpoint'     => NULL,
            'slingshot_token'        => NULL,
            'slingshot_cypher_key'   => NULL,
        ]);
    }
}
