<?php

use Illuminate\Database\Seeder;
use App\Models\BannerType;

class BannerTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BannerType::create([
            'name'       => 'Thank You',
            'active' => 1
        ]);

        BannerType::create([
            'name'       => 'Mesh',
            'active' => 1
        ]);

        BannerType::create([
            'name'       => 'Featured Image',
            'active' => 1
        ]);

        BannerType::create([
            'name'       => 'Arbitrage',
            'active' => 1
        ]);
    }
}
