<?php

use Illuminate\Database\Seeder;
use App\Models\Theme;

class ThemeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Theme::create([
            'name'       => 'Opera Theme 1',
            'button_text' => 'Welcome',
            'button_colour' => '#344456',
            'text_colour' => '#000',
            'background_colour' => '#990000',
        ]);

        Theme::create([
            'name'       => 'MTN Theme 1',
            'button_text' => 'Welcome',
            'button_colour' => '#344456',
            'text_colour' => '#000',
            'background_colour' => '#990000',
        ]);

        Theme::create([
            'name'       => 'Generic Theme 1',
            'button_text' => 'Wassup!',
            'button_colour' => '#000',
            'text_colour' => '#FFF',
            'background_colour' => '#858585',
        ]);
    }
}
