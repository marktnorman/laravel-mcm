<?php

use Illuminate\Database\Seeder;
use App\Models\RuleConditionType;

class RuleConditionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RuleConditionType::create([
            'name'       => 'does contain',
            'class_name' => 'DoesContainConditionType',
            'has_value'  => 1,
        ]);

        RuleConditionType::create([
            'name'       => 'does not contain',
            'class_name' => 'DoesNotContainConditionType',
            'has_value'  => 1,
        ]);

        RuleConditionType::create([
            'name'       => 'does exist',
            'class_name' => 'DoesExistConditionType',
            'has_value'  => 0,
        ]);

        RuleConditionType::create([
            'name'       => 'does not exist',
            'class_name' => 'DoesNotExistConditionType',
            'has_value'  => 0,
        ]);

        RuleConditionType::create([
            'name'       => 'is',
            'class_name' => 'IsConditionType',
            'has_value'  => 1,
        ]);

        RuleConditionType::create([
            'name'       => 'is not',
            'class_name' => 'IsNotConditionType',
            'has_value'  => 1,
        ]);

        RuleConditionType::create([
            'name'       => 'is empty',
            'class_name' => 'IsEmptyConditionType',
            'has_value'  => 0,
        ]);

        RuleConditionType::create([
            'name'       => 'is not empty',
            'class_name' => 'IsNotEmptyConditionType',
            'has_value'  => 0,
        ]);

        RuleConditionType::create([
            'name'       => 'is true',
            'class_name' => 'IsTrueConditionType',
            'has_value'  => 0,
        ]);

        RuleConditionType::create([
            'name'       => 'is false',
            'class_name' => 'IsFalseConditionType',
            'has_value'  => 0,
        ]);

        RuleConditionType::create([
            'name'       => 'less than',
            'class_name' => 'LessThanConditionType',
            'has_value'  => 1,
        ]);

        RuleConditionType::create([
            'name'       => 'greater than',
            'class_name' => 'GreaterThanConditionType',
            'has_value'  => 1,
        ]);
    }
}
