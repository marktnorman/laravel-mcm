<?php

use Illuminate\Database\Seeder;
use App\Models\BannerVariation;

class BannerVariationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BannerVariation::create([
            'banner_id' => 1,
            'banner_variation_type_id' => 3,
            'width' => 1024,
            'height' => 768,
            'location' => 'http://s3.somelocation.blabla.dee3d123',
        ]);

        BannerVariation::create([
            'banner_id' => 1,
            'banner_variation_type_id' => 3,
            'width' => 768,
            'height' => 520,
            'location' => 'http://s3.somelocation.blabla.dee3d122',
        ]);

        BannerVariation::create([
            'banner_id' => 1,
            'banner_variation_type_id' => 1,
            'width' => 220,
            'height' => 160,
            'location' => 'http://s3.somelocation.blabla.dee3d121',
        ]);

        BannerVariation::create([
            'banner_id' => 2,
            'banner_variation_type_id' => 3,
            'width' => 1024,
            'height' => 768,
            'location' => 'http://s3.somelocation.blabla.dee3d323',
        ]);

        BannerVariation::create([
            'banner_id' => 2,
            'banner_variation_type_id' => 3,
            'width' => 768,
            'height' => 520,
            'location' => 'http://s3.somelocation.blabla.dee3d322',
        ]);

        BannerVariation::create([
            'banner_id' => 2,
            'banner_variation_type_id' => 2,
            'width' => 520,
            'height' => 360,
            'location' => 'http://s3.somelocation.blabla.dee3d321',
        ]);

        BannerVariation::create([
            'banner_id' => 3,
            'banner_variation_type_id' => 3,
            'width' => 1024,
            'height' => 768,
            'location' => 'http://s3.somelocation.blabla.dee3d423',
        ]);

        BannerVariation::create([
            'banner_id' => 3,
            'banner_variation_type_id' => 3,
            'width' => 768,
            'height' => 520,
            'location' => 'http://s3.somelocation.blabla.dee3d422',
        ]);

        BannerVariation::create([
            'banner_id' => 3,
            'banner_variation_type_id' => 1,
            'width' => 220,
            'height' => 120,
            'location' => 'http://s3.somelocation.blabla.dee3d421',
        ]);

        BannerVariation::create([
            'banner_id' => 4,
            'banner_variation_type_id' => 3,
            'width' => 1024,
            'height' => 768,
            'location' => 'http://s3.somelocation.blabla.dee3d523',
        ]);

        BannerVariation::create([
            'banner_id' => 4,
            'banner_variation_type_id' => 3,
            'width' => 768,
            'height' => 520,
            'location' => 'http://s3.somelocation.blabla.dee3d522',
        ]);

        BannerVariation::create([
            'banner_id' => 4,
            'banner_variation_type_id' => 2,
            'width' => 520,
            'height' => 360,
            'location' => 'http://s3.somelocation.blabla.dee3d521',
        ]);

        BannerVariation::create([
            'banner_id' => 5,
            'banner_variation_type_id' => 3,
            'width' => 1024,
            'height' => 768,
            'location' => 'http://s3.somelocation.blabla.dee3d623',
        ]);

        BannerVariation::create([
            'banner_id' => 5,
            'banner_variation_type_id' => 3,
            'width' => 768,
            'height' => 520,
            'location' => 'http://s3.somelocation.blabla.dee3d622',
        ]);

        BannerVariation::create([
            'banner_id' => 5,
            'banner_variation_type_id' => 1,
            'width' => 320,
            'height' => 160,
            'location' => 'http://s3.somelocation.blabla.dee3d621',
        ]);
    }
}
