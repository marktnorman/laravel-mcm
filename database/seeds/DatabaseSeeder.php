<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(BillingChannelsTableSeeder::class);
         $this->call(CountriesTableSeeder::class);
         $this->call(ThemeTableSeeder::class);
         $this->call(TemplateTableSeeder::class);
         $this->call(ServicesTableSeeder::class);
         $this->call(NetworksTableSeeder::class);
         $this->call(BillingChannelServiceRelation::class);
         $this->call(BillingChannelNetworkRelation::class);
         $this->call(UserTableSeeder::class);
         $this->call(RuleConditionTypesTableSeeder::class);
         $this->call(RuleMetricTypesTableSeeder::class);
         $this->call(RuleDimensionTypesTableSeeder::class);
         $this->call(RuleTableSeeder::class);
         $this->call(RuleConditionsTableSeeder::class);
         $this->call(ConditionTypeMetricTypeSeeder::class);
         $this->call(BannerTypesTableSeeder::class);
         $this->call(BannersTableSeeder::class);
         $this->call(BannerVariationTypesSeeder::class);
         $this->call(BannerVariationsTableSeeder::class);
         $this->call(ServiceBannerTableSeeder::class);

    }
}
