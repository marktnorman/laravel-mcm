<?php

use Illuminate\Database\Seeder;
use App\Models\Template;

class TemplateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Template::create([
            'name'       => 'Opera Template',
            'theme_id' => 1,
            'upsell' => 0,
        ]);

        Template::create([
            'name'       => 'Vodacom Template',
            'theme_id' => 3,
            'upsell' => 0,
        ]);

        Template::create([
            'name'       => 'MTN Template',
            'theme_id' => 2,
            'upsell' => 1,
        ]);
    }
}
