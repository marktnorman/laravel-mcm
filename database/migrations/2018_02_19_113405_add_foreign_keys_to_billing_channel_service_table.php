<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBillingChannelServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('billing_channel_service', function(Blueprint $table)
		{
			$table->foreign('billing_channel_id', 'fk_table2_billing_channels1')->references('id')->on('billing_channels')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('service_id', 'fk_table2_services')->references('id')->on('services')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('billing_channel_service', function(Blueprint $table)
		{
			$table->dropForeign('fk_table2_billing_channels1');
			$table->dropForeign('fk_table2_services');
		});

        Schema::dropIfExists('billing_channel_service');
	}

}
