<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerVariationTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_variation_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',150);
            $table->integer('min_width');
            $table->integer('max_width');
            $table->integer('min_height');
            $table->integer('max_height');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_variation_types');
    }
}
