<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyServiceBannerPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banner_service', function(Blueprint $table)
        {
            $table->foreign('banner_id', 'fk_pt1_banners')->references('id')->on('banners')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('service_id', 'fk_pt1_services')->references('id')->on('services')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banner_service', function(Blueprint $table)
        {
            $table->dropForeign('fk_pt1_banners');
            $table->dropForeign('fk_pt1_services');
        });

        Schema::dropIfExists('banner_service');
    }
}
