<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToRuleConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */



    public function up()
    {
        Schema::table('rule_conditions', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->foreign('rule_id')->references('id')->on('rules')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('metric_type_id')->references('id')->on('rule_metric_types')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('condition_type_id')->references('id')->on('rule_condition_types')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('dimension_type_id')->references('id')->on('rule_dimension_types')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rule_conditions', function(Blueprint $table)
        {
            $table->dropForeign(['rule_id']);
            $table->dropForeign(['metric_type_id']);
            $table->dropForeign(['condition_type_id']);
            $table->dropForeign(['dimension_type_id']);
        });
    }
}
