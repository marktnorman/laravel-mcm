<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillingChannelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('billing_channels', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->integer('sdp_billing_channel_id');
			$table->string('name', 100)->unique();
			$table->string('title', 100);
			$table->boolean('active')->default('0');
            $table->boolean('internal_doi')->default('0');
            $table->boolean('forced_wap_doi')->default('0');
            $table->string('slingshot_endpoint', 100)->nullable();
            $table->string('slingshot_token', 100)->nullable();
            $table->string('slingshot_cypher_key', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('billing_channels');
	}

}
