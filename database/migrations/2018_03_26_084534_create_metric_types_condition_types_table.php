<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetricTypesConditionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rule_condition_type_rule_metric_type', function (Blueprint $table) {
            $table->bigInteger('rule_metric_type_id')->unsigned();
            $table->bigInteger('rule_condition_type_id')->unsigned();
            $table->timestamps();
            $table->primary(['rule_metric_type_id','rule_condition_type_id'], 'met_con_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rule_condition_type_rule_metric_type');
    }
}
