<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyBannerVariationType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banner_variations', function(Blueprint $table)
        {
            $table->foreign('banner_id')->references('id')->on('banners')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('banner_variation_type_id')->references('id')->on('banner_variation_types')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banner_variations', function(Blueprint $table)
        {
            $table->dropForeign(['banner_id']);
            $table->dropForeign(['banner_variation_type_id']);
        });
    }
}

