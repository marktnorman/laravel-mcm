<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRuleConditions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rule_conditions', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->bigInteger('rule_id')->unsigned();
            $table->string('operator', 30);
            $table->bigInteger('metric_type_id')->unsigned();
            $table->bigInteger('condition_type_id')->unsigned();
            $table->bigInteger('dimension_type_id')->unsigned()->nullable();
            $table->string('value', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rule_conditions');
    }
}
