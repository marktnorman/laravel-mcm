<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillingChannelNetworkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('billing_channel_network', function(Blueprint $table)
		{
			$table->bigInteger('billing_channel_id')->unsigned()->index('fk_table3_billing_channels1_idx');
			$table->bigInteger('network_id')->unsigned()->index('fk_table3_networks1_idx');
            $table->timestamps();
			$table->primary(['network_id','billing_channel_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('billing_channel_network');
	}

}
