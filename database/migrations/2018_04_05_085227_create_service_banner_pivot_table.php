<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceBannerPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_service', function (Blueprint $table) {
            $table->bigInteger('service_id')->unsigned();
            $table->bigInteger('banner_id')->unsigned();
            $table->timestamps();
            $table->primary(['service_id','banner_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_service');
    }
}
