<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBillingChannelNetworkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('billing_channel_network', function(Blueprint $table)
		{
			$table->foreign('billing_channel_id', 'fk_table3_billing_channels1')->references('id')->on('billing_channels')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('network_id', 'fk_table3_networks1')->references('id')->on('networks')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('billing_channel_network', function(Blueprint $table)
		{
			$table->dropForeign('fk_table3_billing_channels1');
			$table->dropForeign('fk_table3_networks1');
		});

        Schema::dropIfExists('billing_channel_network');
	}

}
