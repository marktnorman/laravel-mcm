<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetricTypesDimensionTypesForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rule_dimension_types', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->foreign('metric_type_id')->references('id')->on('rule_metric_types')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('rule_dimension_types', function(Blueprint $table)
        {
            $table->dropForeign(['metric_type_id']);
        });
    }
}
