<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateServicesTable
 *
 * @author Vince Kruger <vincekruger@gmail.com>
 */
class CreateServicesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('country_id')->unsigned();
            $table->bigInteger('template_id')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('sdp_service_id')->unsigned();
            $table->string('name', 100)->unique();
            $table->string('title', 150);
            $table->string('campaign_keyword', 100);
            $table->text('service_copy');
            $table->string('service_cost', 100);
            $table->string('display_title', 100);
            $table->string('post_doi_landing_url', 250);
            $table->string('forgot_password_url', 250);
            $table->tinyInteger('double_optin');
            $table->tinyInteger('display_interstitial');
            $table->boolean('active');
            $table->timestamps();
        });
    }


    /**vices
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }

}
