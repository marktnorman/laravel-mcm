<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNetworksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('networks', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->string('name', 100);
			$table->string('title', 100);
			$table->boolean('active');
			$table->text('ip_range', 65535)->nullable();
			$table->boolean('slingshot_enabled')->default(0);
			$table->string('slingshot_url', 100)->nullable();
			$table->string('slingshot_token', 32)->nullable();
			$table->string('slingshot_cypher_key', 64)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('networks');
	}

}
