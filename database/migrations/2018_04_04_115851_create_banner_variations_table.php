<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_variations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('banner_id')->unsigned();
            $table->bigInteger('banner_variation_type_id')->unsigned();
            $table->integer('width');
            $table->integer('height');
            $table->string('location', 250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_variations');
    }
}
