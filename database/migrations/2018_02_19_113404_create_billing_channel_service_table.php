<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillingChannelServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('billing_channel_service', function(Blueprint $table)
		{
			$table->bigInteger('billing_channel_id')->unsigned()->index('fk_table2_billing_channels1_idx');
			$table->bigInteger('service_id')->unsigned()->index('fk_table2_services_idx');
            $table->timestamps();
			$table->primary(['billing_channel_id','service_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('billing_channel_service');
	}

}
